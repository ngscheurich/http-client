﻿
# HTTP Client

HTTP Client is a test project for Commodore 64/128 to send requests via HTTP. GET and POST requests are supported at the moment. Project is under construction, and a lab for
CommodoRex game (under development). It was tested with 64NIC+ (RR-Net type) ethernet card.


## Contents:

- original Http-Load library written by Doc Bacardi / The Dreams
- custom POST method add-on library written by Bartosz Żołyński / Commocore (based on the original Http-Load) with ability to send custom body requests in very easy way
- example for both methods, and some debugging options
- DHCP taken from Contiki to detect dynamic IP address
- HTTP Client uses 6502 JSON Parser library to parse JSON response from API: [https://bitbucket.org/Commocore/6502-json-parser](https://bitbucket.org/Commocore/6502-json-parser)


## Installation

HTTP Client uses [6502 JSON Parser library](https://bitbucket.org/Commocore/6502-json-parser) which needs to be installed as vendor dependency.
Simply run `6502-json-parser-dependency.bat` script (Windows), or `6502-json-parser-dependency.sh` (*nix) to clone it from remote git repository.


## Configuration

You can use Contiki's DHCP (in dhcp folder) to obtain dynamic IP address, however you have to compile settings file `IPCONFIG-DEFAULT` with given IP address manually.
To do so, just copy file `src/ipconfig-default.asm.dist` to `src/ipconfig-default.asm`, then set up addresses from DHCP and compile it using `compile-ipconfig-default.bat` script
(Windows), or `compile-ipconfig-default.sh` script (*nix). This file is loaded on the start up of HTTP Client, and if it's not found, default settings from the main program will be used.


## Compiling

Use `compile.bat` for Windows, or `compile.sh` for *nix.
		

## Notes

When the total response time is faster than 200ms, it might result in HTTP Client hangs.
This is because the HTTP Client program may not be ready to receive a response yet.
A safety net is to set a delay for at least 200ms.

E.g. in PHP it might be achieved this way:

```php
usleep(200000);
```


## License.

Copyright © 2017 - 2019, Bartosz Żołyński, [Commocore](http://www.commocore.com).

Released under the [GNU General Public License, version 3](LICENSE).
