﻿
; @access public
; @param X - lo-byte for request body memory location
; @param Y - hi-byte for request body memory location
; @return void
initBodyMemoryLocation
	stx requestBodyPointerStart
	sty requestBodyPointerStart + 1
	stx requestBodyPointer + 1
	sty requestBodyPointer + 2
rts


; @access public
; @param Y - size
; @return void
addStringToBodyRequest
-
copyStringPointer
	lda $1234

requestBodyPointer
	sta $1234

	inc copyStringPointer + 1
	bne +
		inc copyStringPointer + 2
+

	inc requestBodyPointer + 1
	bne +
		inc requestBodyPointer + 2
+

	inc requestBodySize
	bne +
		inc requestBodySize + 1
+
	
	dey
	bne -
rts


; @access public
; @param A - byte
; @return void
addByteToBodyRequest
	ldy #1
	jmp requestBodyPointer
rts


requestBodyPointerStart
	.word 0
	
	
requestBodySize
	.word 0
