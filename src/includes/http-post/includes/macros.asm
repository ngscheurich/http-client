﻿
; @param word memoryLocation
setBodyMemoryLocation .macro memoryLocation
	ldx #<\memoryLocation
	ldy #>\memoryLocation
	jsr HttpPostMethod.initBodyMemoryLocation
.endm


; @param word stringLocation
; @param byte length
addStringToBody .macro stringLocation, length
	lda #<\stringLocation
	sta HttpPostMethod.copyStringPointer + 1
	lda #>\stringLocation
	sta HttpPostMethod.copyStringPointer + 2
	ldy #\length
	jsr HttpPostMethod.addStringToBodyRequest
.endm


addByteToBody .macro
	jsr HttpPostMethod.addByteToBodyRequest
.endm
