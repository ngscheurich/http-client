
; POST method implementation
; This is a copy-paste version of GET method with additional request body implementation and few changes
; Originally written as the part of Http-Load library by Doc Bacardi / The Dreams
; 
; @access public
; @return carry flag set on error (e.g. bad request)
HttpPostMethod .proc
	.if HTTP_SEND_POST_BUFFER_TEST_LOCATION
		; Allocate memory for buffer for testing purposes
		lda #<HTTP_SEND_POST_BUFFER_TEST_LOCATION
		sta BucketZp_BufPtr
		lda #>HTTP_SEND_POST_BUFFER_TEST_LOCATION
		sta BucketZp_BufPtr + 1
		jmp http_sendPost
	.fi
	lda #10
	sta maxredirect

restart
	lda $d012 ;#<32772
	ldx #>32772
	jsr tcp_setSrcPort

	lda #<80
	ldx #>80
	jsr tcp_setDstPort

	lda #HttpState_Open
	sta HttpZp_State

	ldy DstIpIdx
	jsr tcp_connect
	bcs http_error

	; Send the request
	jsr http_sendPost
	bcs http_error

	jsr http_getLine
	bcc http_testReply
http_error
	sec
	rts

http_testReply
	lda HttpZp_LineLen
	cmp #14
	bcc notFound

	ldx #header0_len - 1
test0
	lda header0,x
	cmp Http_Line,x
	bne notFound
	dex
	bpl test0

	; test for 0 or 1
	lda Http_Line+header0_len
	and #$fe
	cmp #"0"
	bne notFound

	ldx #header1_len - 1
-
	lda header1,x
	cmp Http_Line+header0_len + 1,x
	bne notOK
	dex
	bpl -

-
	jsr http_getLine
	bcs notFound
	txa
	bne -

	clc
	rts
notOK
	dec maxredirect
	beq notFound

	ldx #header2_len - 1
-
	lda header2,x
	cmp Http_Line + header0_len + 1,x
	bne notFound
	dex
	bpl -

lp
	jsr http_getLine
	bcs notFound
	txa
	beq notFound
	sta u + 1

	ldx #header3_len - 1
-
	lda header3,x
	cmp Http_Line,x
	bne lp
	dex
	bpl -

	ldy #0
	ldx #header3_len
-
		lda Http_Line,x
		sta fileName,y
		iny
		inx
u
		cpx #0
	blt -
	sty fileName_len

	jsr tcp_disconnect2

	jsr parseHostName
	jmp restart

notFound
	;TODO: test for other reply codes and generate a more reasonable error message than "file not found"

	sec
	rts

header0
	.text "HTTP/1."
header0_len = * - header0

header1
	.text " 200"
header1_len = * - header1

header2
	.text " 30"
header2_len = * - header2

header3
	.text "Location: http://"
header3_len = * - header3

maxredirect     .byte 0



; @access private
; @return void
http_sendPost	
	lda #0
	sta bucketLength
	sta bucketLength + 1

	; Check for request body if any
	lda requestBodySize
	ora requestBodySize + 1
	beq noRequestBody
		; There is some body, let's count the length of Content-Length value
		lda requestBodySize
		sta numberLo
		lda requestBodySize + 1
		sta numberHi
		jsr extract16bitValueToDec
		
		; Check length of string for Content-Length value
		ldy #0
		lda number1
		beq +
			ldy #1
+
		lda number10
		beq +
			ldy #2
+
		lda number100
		beq +
			ldy #3
+
		lda number1000
		beq +
			ldy #4
+
		lda number10000
		beq +
			ldy #5
+
		sty contentLengthNumberPositions + 1
		sty bucketLength
		
		; Add request size
		lda requestBodySize
		clc
		adc bucketLength
		sta bucketLength
		
		lda requestBodySize + 1
		adc bucketLength + 1
		sta bucketLength + 1
		
		; Reset request body size for future requests (zeroing as next request could not have any body set up)
		lda requestBodySize
		sta requestBodySizeInternal
		
		lda requestBodySize + 1
		sta requestBodySizeInternal + 1
		
		lda #0
		sta requestBodySize
		sta requestBodySize + 1
		
noRequestBody

	lda #<(http_postCmd0_length + http_postCmd1_length + http_postCmd2_length + http_postCmd3_length)
	clc
	adc bucketLength
	sta bucketLength
	
	lda #>(http_postCmd0_length + http_postCmd1_length + http_postCmd2_length + http_postCmd3_length)
	adc bucketLength + 1
	sta bucketLength + 1
	
	lda bucketLength
	clc
	adc fileName_len
	sta bucketLength
	
	lda bucketLength + 1
	adc #0
	sta bucketLength + 1
	tay

	.if !HTTP_SEND_POST_BUFFER_TEST_LOCATION
		lda bucketLength
		jsr Bucket_Alloc
		bcs error0
		stx HttpZp_BucketOut
	.fi

	ldy #http_postCmd0_length - 1
-
	lda http_postCmd0,y
	sta (BucketZp_BufPtr),y
	dey
	bpl -

	lda BucketZp_BufPtr
	clc
	adc #http_postCmd0_length
	sta BucketZp_BufPtr
	bcc +
		inc BucketZp_BufPtr + 1
+

	jsr setHostnameUri

	tya
	jsr addBytePtrToBufferPtr

	ldy #http_postCmd1_length - 1
-	lda http_postCmd1,y
	sta (BucketZp_BufPtr),y
	dey
	bpl -

	lda BucketZp_BufPtr
	clc
	adc #http_postCmd1_length
	sta BucketZp_BufPtr
	bcc +
		inc BucketZp_BufPtr + 1
+

	; Set Host
	jsr setHostHeader

	; Move forward...
	lda #http_postCmd2_length - 1
	clc
	adc BucketZp_BufPtr
	sta BucketZp_BufPtr
	bcc +
		inc BucketZp_BufPtr + 1
+

	; Add Content-Length value
	lda requestBodySizeInternal
	ora requestBodySizeInternal + 1
	beq +
contentLengthNumberPositions
		ldx #0 ; code modified on fly
		ldy #0		
-
		lda number1 - 1,x
		clc
		adc #48
		sta (BucketZp_BufPtr),y
		iny
		dex
		bne -
		lda contentLengthNumberPositions + 1
		jsr addBytePtrToBufferPtr
		jmp ++
+
	ldy #0
	lda #48 ; set "0" for Content-Length value
	sta (BucketZp_BufPtr),y
	lda #1
	jsr addBytePtrToBufferPtr
+
	
	; Add two breaklines
	ldy #0
-
	lda http_postCmd3,y
	sta (BucketZp_BufPtr),y
	iny
	cpy #4
	bne -
	
	tya
	jsr addBytePtrToBufferPtr
	
	; Add request body
	jsr addBodyRequestToBuffer
	
	; Finish
	ldx HttpZp_BucketOut
	jmp tcp_sendData

error0
	sec
	rts


; @access private
; @return void
setHostnameUri
	ldx HostNameLength
	ldy #0
-
	lda fileName,x
	sta (BucketZp_BufPtr),y
	iny
	inx
	cpx fileName_len
	bcc -
rts


; @access private
; @return void
setHostHeader
	lda HostNameLength
	beq +
		ldy #0
-
		lda fileName,y
		sta (BucketZp_BufPtr),y
		iny
		cpy HostNameLength
		bcc -

		tya
		clc
		adc BucketZp_BufPtr
		sta BucketZp_BufPtr
		bcc +
			inc BucketZp_BufPtr + 1
+

	ldy #http_postCmd2_length - 1
-
	lda http_postCmd2,y
	sta (BucketZp_BufPtr),y
	dey
	bpl -
rts


; @access private
; @return void
addBodyRequestToBuffer
	lda requestBodyPointerStart
	sta copyBodyRequestFrom + 1
	lda requestBodyPointerStart + 1
	sta copyBodyRequestFrom + 2

	ldy #0
-
copyBodyRequestFrom
	lda $1234
	sta (BucketZp_BufPtr),y
	
	lda #1
	jsr addBytePtrToBufferPtr
	
	inc copyBodyRequestFrom + 1
	bne +
		inc copyBodyRequestFrom + 2
+

	lda copyBodyRequestFrom + 1
	cmp requestBodyPointer + 1
	bne -
	lda copyBodyRequestFrom + 2
	cmp requestBodyPointer + 2
	bne -
	
	; Add two breaklines at the end of body request
	ldy #0
-
	lda http_postCmd3,y
	sta (BucketZp_BufPtr),y
	iny
	cpy #2
	bne -
	
	lda #2
	jsr addBytePtrToBufferPtr
rts


; @access private
; @param A - byte of offset added to pointer of buffer
; @return void	
addBytePtrToBufferPtr
	clc
	adc BucketZp_BufPtr
	sta BucketZp_BufPtr
	bcc +
		inc BucketZp_BufPtr + 1
+
rts


bucketLength
	.word 0
	

requestBodySizeInternal
	.word 0
	

http_postCmd0
	.text "POST "
http_postCmd0_length = * - http_postCmd0

http_postCmd1
	.text " HTTP/1.0", $0d, $0a
	.text "User-Agent: "
	.text httpUserAgent
	.text " ("
	.if SUPERRAM
        .text "SuperCPU "
	.fi
	.if C128
        .text "Commodore 128"
	.else
        .text "Commodore 64"
	.fi
	.text")", $0d, $0a
	.text "Content-Type: application/x-www-form-urlencoded", $0d, $0a
	.text "Host: "
http_postCmd1_length = * - http_postCmd1

http_postCmd2
	.text $0d, $0a
	.text "Connection: close", $0d, $0a
	.text "Content-Length: "
http_postCmd2_length = * - http_postCmd2

http_postCmd3
	.text $0d, $0a, $0d, $0a
http_postCmd3_length = * - http_postCmd3
	
	; Includes
	.include "includes/http-post-method-handler.asm"	
	.include "includes/extract-16-bit-value-to-dec.asm"
	.include "includes/variables.asm"
	.include "includes/macros.asm"

	.pend
