
; @access public
; @affects fileName
; @affects fileName_len
; @return void
setUri
	ldx #255
-
	inx
	lda hiScoresUrl,x
	sta fileName,x
	cmp #13
	bne -
	stx fileName_len
rts
