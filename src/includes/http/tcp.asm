
;--------------------------------------

TcpCfg_RetryNum = 5

TcpCfg_ConnectTimeout = 5		; connect response
TcpCfg_DisconnectTimeout = 5		; disconnect response
TcpCfg_AckTimeout = 5			; ack packet for sent data
TcpCfg_GetDataTimeout = 5		; receive data

;--------------------------------------

TcpHeader_SrcPort = $00
TcpHeader_DstPort = $02
TcpHeader_SeqNr = $04
TcpHeader_AckNr = $08
TcpHeader_DataOffs = $0c
TcpHeader_Flags = $0d
TcpHeader_WindowSize = $0e
TcpHeader_Checksum = $10
TcpHeader_UrgentPtr = $12
TcpHeader_Options = $14			; fixed to 4 byte
TcpHeader_length = $18

Tcp_Data	= $18

;--------------------------------------
; psy ip header for tcp checksum

PsyIpV4Header_SrcIp = $00
PsyIpV4Header_DstIp = $04
PsyIpV4Header_Zero = $08
PsyIpV4Header_Proto = $09
PsyIpV4Header_Size = $0a
PsyIpV4Header_length = $0c

;--------------------------------------

TcpCfg_WindowSize = $0300		; bytes which can be send without ack
TcpCfg_SegmentSize = $0180		; max segment size

;--------------------------------------

;	.segment "data"
		.comment
TcpZp_A		.byte 0
TcpZp_X		.byte 0
TcpZp_Y		.byte 0

TcpZp_State	.byte 0
TcpZp_Flags	.byte 0

TcpZp_BucketIn	.byte 0
TcpZp_BucketOut .byte 0
TcpZp_BucketChecksumIn .byte 0
TcpZp_BucketChecksumPsy .byte 0
Tcp_SrcPort	*= *+2
Tcp_DstPort	*= *+2
Tcp_ISN		*= *+4			; Initial Sequence Number
Tcp_CSN		*= *+4			; Current Sequence Number
Tcp_ASN		*= *+4			; Acknowledged Sequence Number
Tcp_PacketLength *= *+2			; Length of the TCP Data part
Tcp_Stack	*= *+8
Tcp_StackSeqLo	*= *+8
Tcp_StackSeqHi	*= *+8

Tcp_GetOffs_FirstUnAckd .word 0
Tcp_GetOffs_LastUnAckd .word 0
Tcp_GetSeq_First *= *+4
Tcp_RecDataLength .word 0
Tcp_GetStackSeq .word 0

TcpZp_MoveStack .byte 0
Tcp_DataOffset	.byte 0
Tcp_RecDataBucket .byte 0

TcpZp_GetBucket .byte 0			;Bucket with valid input data, $ff means no bucket
TcpZp_GetLength .word 0			;Length of the GetBucket (to save the call to Bucket_Len for every get operation)
TcpZp_GetOffset .word 0			;Offset in the GetBucket

TcpZp_Checksum	.word 0

TcpZp_ConnectedIpIdx .byte 0
TcpZp_SrcIpIdx	.byte 0
TcpZp_DstIpIdx	.byte 0

TcpZp_Retries	.byte 0
TcpZp_TimeOutBuffer *= *+4
		.endc
;--------------------------------------

;	.segment "bank0"

TcpState_Idle	= 0
TcpState_SynSent = 1
TcpState_Connected = 2
TcpState_Transfer = 3
TcpState_CloseSent = 4


TcpFlag_FIN	= $01
TcpFlag_SYN	= $02
TcpFlag_RST	= $04
TcpFlag_PSH	= $08
TcpFlag_ACK	= $10

TCP_WindowSize	= 7

;--------------------------------------

tcp_init
		lda #TcpState_Idle
		sta TcpZp_State
		lda #$ff		;no GetBucket
		sta TcpZp_GetBucket
		sta TcpZp_GetLength
		sta TcpZp_GetLength+1
		lda #0
		sta $3fe
		rts

;--------------------------------------

tcp_setSrcPort
		sta Tcp_SrcPort+1
		stx Tcp_SrcPort
		rts

;--------------------------------------

tcp_setDstPort
		sta Tcp_DstPort+1
		stx Tcp_DstPort
		rts

;--------------------------------------

tcp_connect	.proc
		sty TcpZp_ConnectedIpIdx

	; clear ASN
		ldx #3
		lda #0
-		sta Tcp_ASN,x
		dex
		bpl -

	; next time we meet the syn packet is sent
		lda #TcpState_SynSent
		sta TcpZp_State

		lda #TcpCfg_RetryNum
		sta TcpZp_Retries

resendPacket
		lsr chksumerr
	; Flags: SYN
		lda #TcpFlag_SYN
		jsr tcp_send_nodata	; no additional data for the packet

	; timeout for receive operation
		ldx #<TcpZp_TimeOutBuffer
		ldy #>TcpZp_TimeOutBuffer
		jsr timer_get

	; receive tcp packet
wait4ConnectReply
		jsr ipv4_get
		bcc gotReplyPacket
		lda chksumerr
		bmi +

		lda #TcpCfg_ConnectTimeout
		ldx #<TcpZp_TimeOutBuffer
		ldy #>TcpZp_TimeOutBuffer
		jsr timer_check
		bcc wait4ConnectReply
+
		dec TcpZp_Retries
		bne resendPacket

		sec
		rts

gotReplyPacket
		jsr tcp_nextCSN

	; test if ack is csn
		stx TcpZp_BucketIn
		jsr Bucket_GetAdr

	; has packet SYN and ACK flags set?
	; TODO: check other flags for error reason (like RST, TCP Zero Window, etc)
		ldy #TcpHeader_Flags
		lda (BucketZp_BufPtr),y
		cmp #TcpFlag_SYN|TcpFlag_ACK
		bne errorTcpConnectionRefused

		ldy #TcpHeader_AckNr
-		lda Tcp_CSN-TcpHeader_AckNr,y
		cmp (BucketZp_BufPtr),y
		bne error0
		iny
		cpy #TcpHeader_AckNr+4
		blt -

		ldx #3
		ldy #TcpHeader_SeqNr+3
		sec
-		lda (BucketZp_BufPtr),y
		adc #0
		sta Tcp_ASN,x
		dey
		dex
		bpl -

		ldx TcpZp_BucketIn
		jsr Bucket_Free

		lda #TcpState_Connected
		sta TcpZp_State

	; Flags: ACK
		jsr tcp_send_ack	; no additional data for the packet
		bcs error1

		lda #0
		sta Tcp_RecDataLength
		sta Tcp_RecDataLength+1
		sta Tcp_RecDataLength+2

		ldx #3
-		lda Tcp_ASN,x
		sta Tcp_GetSeq_First,x
		dex
		bpl -
		stx Tcp_Stack

		clc
		rts

errorTcpConnectionRefused
error0
		ldx TcpZp_BucketIn
		jsr Bucket_Free
error1
		sec
		rts

		.pend

;--------------------------------------
tcp_disconnect2 .proc
		jsr tcp_send_rst
		jmp tcp_disconnect.in
		.pend

tcp_disconnect	.proc
	; Flags: ACK
;	 lda #TcpFlag_ACK|TcpFlag_FIN
 ;	 jsr tcp_send_nodata	  ; no additional data for the packet
in
	;free the rest of the stack
		ldx Tcp_Stack
		bmi +
		jsr Bucket_Free
+
		ldx TcpZp_GetBucket
		bmi lastBucketFreed
		jsr Bucket_Free

		ldx #$ff
		stx TcpZp_GetBucket
lastBucketFreed
		stx TcpZp_GetLength
		stx TcpZp_GetLength+1
		stx Tcp_Stack

		clc
		jmp tcp_init
		.pend

;--------------------------------------

tcp_nextCSN	.proc
	;inc csn
		inc Tcp_CSN+3
		bne nextCsn0
		inc Tcp_CSN+2
		bne nextCsn0
		inc Tcp_CSN+1
		bne nextCsn0
		inc Tcp_CSN
nextCsn0
		rts
		.pend

;--------------------------------------

tcp_sendData	.proc
		lda #TcpCfg_RetryNum
		sta TcpZp_Retries

resendPacket
		lsr chksumerr
		lda #TcpFlag_PSH|TcpFlag_ACK
		jsr tcp_send

	; timeout for receive operation
		ldx #<TcpZp_TimeOutBuffer
		ldy #>TcpZp_TimeOutBuffer
		jsr timer_get

	; receive tcp packet
getPacket
		jsr ipv4_get
		bcs error2

	; test if ack is csn
		stx TcpZp_BucketIn
		jsr Bucket_GetAdr

	; has packet ACK flag set?
	; TODO: check other flags for error reason (like RST, TCP Zero Window, etc)
		ldy #TcpHeader_Flags
		lda (BucketZp_BufPtr),y
		cmp #TcpFlag_ACK
		bne error3

	;does the seq number match -> accept the new ack value even if it's far too high
		ldy #TcpHeader_SeqNr
-		lda Tcp_ASN-TcpHeader_SeqNr,y
		cmp (BucketZp_BufPtr),y
		bne error3
		iny
		cpy #TcpHeader_SeqNr+4
		blt -

		ldy #TcpHeader_AckNr
-		lda (BucketZp_BufPtr),y
		sta Tcp_CSN-TcpHeader_AckNr,y
		iny
		cpy #TcpHeader_AckNr+4
		blt -

		jsr tcp_nextCSN

		ldx TcpZp_BucketIn
		jsr Bucket_Free

		lda #TcpState_Transfer
		sta TcpZp_State

		clc
		rts

error3		ldx TcpZp_BucketIn
		jsr Bucket_Free
error2
		lda chksumerr
		bmi +

		lda #TcpCfg_AckTimeout
		ldx #<TcpZp_TimeOutBuffer
		ldy #>TcpZp_TimeOutBuffer
		jsr timer_check
		bcc getPacket
+
		dec TcpZp_Retries
		bne resendPacket

		sec
		rts
		.pend

;--------------------------------------
; Send tcp datagram
; flags in the header must be set, all others are set here
; parameters are
;  x - bucket index

tcp_send_rst	.proc
		lda #TcpFlag_RST
		.byte $2c
		.cerror tcp_send_ack & 0
		.pend

tcp_send_ack	.proc
		lda #TcpFlag_ACK
		.cerror tcp_send_nodata & 0
		.pend

tcp_send_nodata .proc
		ldx #$ff
		.cerror tcp_send & 0
		.pend

tcp_send	.proc
		stx TcpZp_BucketIn
		sta TcpZp_Flags
		lda #<TcpHeader_length
		ldy #>TcpHeader_length
		cpx #$ff
		beq data0

		jsr Bucket_GrowFront_small
		bcs error1
		stx TcpZp_BucketOut
		gcc data1		;bra

error1
		sec
		rts

data0
		jsr Bucket_Alloc
		bcs error1

data1
		stx TcpZp_BucketOut
		jsr Bucket_GetAdr

		ldy #TcpHeader_DataOffs
	; TCP Dataoffset: 24 Bytes
		lda #24<<2
		sta (BucketZp_BufPtr),y

	;Set Source Port, Destination Port, CSN and ASN
-		lda Tcp_SrcPort-1,y
		dey
		sta (BucketZp_BufPtr),y
		bne -

	; No Urgent pointer
		tya
		ldy #TcpHeader_UrgentPtr
		sta (BucketZp_BufPtr),y
		iny
		sta (BucketZp_BufPtr),y

	; Windowsize is $01ff just for a test
		lda #<TcpCfg_WindowSize
		ldy #TcpHeader_WindowSize+1
		sta (BucketZp_BufPtr),y
		lda #>TcpCfg_WindowSize
		dey
		sta (BucketZp_BufPtr),y

	; test if packet has syn flag set
		dey
		lda TcpZp_Flags
		sta (BucketZp_BufPtr),y
		ldy #TcpHeader_Options
		and #TcpFlag_SYN
		beq tcp_send2
	; Options for Syn Packet
		lda #2			; Max Segment Size
		sta (BucketZp_BufPtr),y
		lda #4			; Size of Element
		iny
		sta (BucketZp_BufPtr),y
		lda #>TcpCfg_SegmentSize
		iny
		sta (BucketZp_BufPtr),y
		lda #<TcpCfg_SegmentSize
		iny
		gne tcp_send3
tcp_send2
		lda #1			; No Option (Fillup)
		sta (BucketZp_BufPtr),y
		iny
		sta (BucketZp_BufPtr),y
		iny
		sta (BucketZp_BufPtr),y
		iny
tcp_send3
		sta (BucketZp_BufPtr),y

		lda MyIpIdx
		sta TcpZp_SrcIpIdx
		lda TcpZp_ConnectedIpIdx
		sta TcpZp_DstIpIdx

		ldx TcpZp_BucketOut
		jsr tcp_outChecksum

	; send the packet
		lda #IpV4_Protocol_Tcp
		ldy TcpZp_DstIpIdx
		jmp ipv4_send
		.pend

;--------------------------------------
getbyte		.proc
	; save registers (a will contain the result, no need so save)
		inc TcpZp_GetLength
		bne packetHere
		inc TcpZp_GetLength+1
		bne packetHere

		dec TcpZp_GetLength
		dec TcpZp_GetLength+1

		sty TcpZp_Y
		stx TcpZp_X
		ldx TcpZp_GetBucket
		bmi nothingLeft
		jsr Bucket_Free

		lda $3fe
		beq nothingLeft
vg		ldx #0
		txs
		rts


nothingLeft
		jsr tcp_getData
		bcs vg
		stx TcpZp_GetBucket
		jsr Bucket_GetAdr
		sta cp+1
		sty cp+2
		jsr Bucket_Len
		eor #255
		sta TcpZp_GetLength
		tya
		eor #255
		sta TcpZp_GetLength+1
		ldx TcpZp_X
		ldy TcpZp_Y
		jmp getbyte

packetHere
cp		lda $ffff

		inc cp+1
		bne +
		inc cp+2
+		clc

	; restore registers
		rts
		.pend


tcp_getData	.proc
getLoop
	; is the first stack element filled?
		ldx Tcp_Stack
		bpl stackHeadFilled	; $ff means stack entry empty
getLoop2
		lsr chksumerr
	; timeout for receive operation
		ldx #<TcpZp_TimeOutBuffer
		ldy #>TcpZp_TimeOutBuffer
		jsr timer_get

waitForNewPacket
	; receive tcp packet
		jsr ipv4_get
		bcc getLoop

		lda chksumerr
		bmi ree

		lda #TcpCfg_GetDataTimeout
		ldx #<TcpZp_TimeOutBuffer
		ldy #>TcpZp_TimeOutBuffer
		jsr timer_check
		bcc waitForNewPacket

error0
		sec
		rts

getLoop2o	jsr Bucket_Free
		lda #$ff
		sta Tcp_Stack
ree		jsr tcp_send_ack
		jmp getLoop2

stackHeadFilled
	; first stack element contains a packet, test the offset
		lda Tcp_StackSeqLo
		cmp Tcp_RecDataLength
		bne getLoop2o
		lda Tcp_StackSeqHi
		cmp Tcp_RecDataLength+1
		bne getLoop2o
		lda Tcp_StackSeqHi2
		cmp Tcp_RecDataLength+2
		bne getLoop2o

	;ldx Tcp_Stack
		jsr Bucket_GetAdr

		ldy #TcpHeader_Flags
		lda (BucketZp_BufPtr),y
		and #TcpFlag_FIN
		beq eee
		dec $3fe
eee

		ldy #TcpHeader_DataOffs
		lda (BucketZp_BufPtr),y
		and #$f0
		lsr
		lsr
		tay

	;ldx Tcp_Stack
		jsr Bucket_ShrinkFront
		bcs error0
		stx Tcp_RecDataBucket

		jsr Bucket_Len
		clc
		adc Tcp_RecDataLength
		sta Tcp_RecDataLength
		tya
		adc Tcp_RecDataLength+1
		sta Tcp_RecDataLength+1
		bcc +
		inc Tcp_RecDataLength+2
+

		lda Tcp_GetSeq_First+3
		clc
		adc Tcp_RecDataLength
		sta Tcp_ASN+3
		lda Tcp_GetSeq_First+2
		adc Tcp_RecDataLength+1
		sta Tcp_ASN+2
		lda Tcp_GetSeq_First+1
		adc Tcp_RecDataLength+2
		sta Tcp_ASN+1
		lda Tcp_GetSeq_First
		adc #0
		sta Tcp_ASN

		lda #$ff
		sta Tcp_Stack

		lda #TcpFlag_ACK
		bit $3fe
		bpl +
		lda #TcpFlag_ACK|TcpFlag_FIN
+
	; send ACK packet
		jsr tcp_send_nodata
		bcs error

		ldx Tcp_RecDataBucket
		rts

error
		sec
		rts
		.pend

;--------------------------------------
; The tcp checksum includes the source
; and destination address, the internet protocol
; and the length of the tcp header + tcp data.
; Note: the length does not include this pseudo
; header.

tcp_outChecksum .proc
		stx TcpZp_BucketChecksumIn
		jsr Bucket_Len
;	bcs psy_error2
		sta Tcp_PacketLength
		sty Tcp_PacketLength+1

	; create psy header
		lda #<PsyIpV4Header_length
		sta MakeChecksumZp_Len
		ldy #>PsyIpV4Header_length
		sty MakeChecksumZp_Len+1
		jsr Bucket_Alloc
		bcs psy_error2

		stx TcpZp_BucketChecksumPsy
		ldy #PsyIpV4Header_Size+1
		lda Tcp_PacketLength
		sta (BucketZp_BufPtr),y
		dey
		lda Tcp_PacketLength+1
		sta (BucketZp_BufPtr),y
		dey
		lda #IpV4_Protocol_Tcp
		sta (BucketZp_BufPtr),y
		dey
		lda #0
		sta (BucketZp_BufPtr),y
		dey

		ldy TcpZp_SrcIpIdx
		lda BucketZp_BufPtr
		ldx BucketZp_BufPtr+1
		jsr ipCache_Get
psy_error2
		bcs error2

		ldy TcpZp_DstIpIdx
		lda #PsyIpV4Header_DstIp
		jsr ipCache_Get_tobucket
		bcs error2

		lda BucketZp_BufPtr
		sta MakeChecksumZp_Ptr
		lda BucketZp_BufPtr+1
		sta MakeChecksumZp_Ptr+1

		ldx TcpZp_BucketChecksumIn
		jsr Bucket_GetAdr

		jsr MakeChecksum

		ldy #TcpHeader_Checksum
		eor #$ff
		sta (BucketZp_BufPtr),y
		iny
		txa
		eor #$ff
		sta (BucketZp_BufPtr),y

		lda BucketZp_BufPtr
		sta MakeChecksumZp_Ptr
		lda BucketZp_BufPtr+1
		sta MakeChecksumZp_Ptr+1

		ldx TcpZp_BucketChecksumIn
		jsr Bucket_Len
		sta MakeChecksumZp_Len
		sty MakeChecksumZp_Len+1

		jsr MakeChecksum

		ldy #TcpHeader_Checksum
		sta (BucketZp_BufPtr),y
		iny
		txa
		sta (BucketZp_BufPtr),y

		ldx TcpZp_BucketChecksumPsy
		jsr Bucket_Free

		ldx TcpZp_BucketChecksumIn
		clc
		rts

error2
		sec
		rts
		.pend

;--------------------------------------

tcp_testInChecksum .proc
	; save packet's checksum to zp
		ldy #TcpHeader_Checksum
		lda (BucketZp_BufPtr),y
		sta TcpZp_Checksum+1
		iny
		lda (BucketZp_BufPtr),y
		sta TcpZp_Checksum

		jsr tcp_outChecksum
		bcs error
		jsr Bucket_GetAdr
		ldy #TcpHeader_Checksum
		lda (BucketZp_BufPtr),y
		cmp TcpZp_Checksum+1
		bne tcp_badInChecksum
		iny
		lda (BucketZp_BufPtr),y
		cmp TcpZp_Checksum
		bne tcp_badInChecksum
		clc
		rts

tcp_badInChecksum
error
		sec
		rts
		.pend

;--------------------------------------
; Get tcp packet

tcp_getPacket	.proc
		stx TcpZp_BucketIn
		sty TcpZp_SrcIpIdx
		jsr Bucket_GetAdr

	; dst ip must be mine (already checked by ipv4 layer)
		lda MyIpIdx
		sta TcpZp_DstIpIdx

		ldy #TcpHeader_DstPort
		lda (BucketZp_BufPtr),y
		cmp Tcp_SrcPort
		bne error
		iny
		lda (BucketZp_BufPtr),y
		cmp Tcp_SrcPort+1
		bne error

	;test checksum
		jsr tcp_testInChecksum
		bcs error2

	; free incoming ip
		ldy TcpZp_SrcIpIdx
		jsr ipCache_Free

		lda TcpZp_State
		cmp #TcpState_Transfer
		bne doneOk

	;still space on tcp stack?
		lda Tcp_Stack
		bmi stackSpace

error2		ror chksumerr
error		ldx TcpZp_BucketIn
		jsr Bucket_Free
		ldy TcpZp_SrcIpIdx
		jsr ipCache_Free
		sec
		rts

stackSpace
		sec
		ldy #TcpHeader_SeqNr+3
		lda (BucketZp_BufPtr),y
		sbc Tcp_GetSeq_First+3
		sta Tcp_StackSeqLo
		dey
		lda (BucketZp_BufPtr),y
		sbc Tcp_GetSeq_First+2
		sta Tcp_StackSeqHi
		dey
		lda (BucketZp_BufPtr),y
		sbc Tcp_GetSeq_First+1
		sta Tcp_StackSeqHi2

		lda TcpZp_BucketIn
		sta Tcp_Stack
doneOk
		clc
		rts

		.pend

chksumerr	.byte 0

;--------------------------------------


