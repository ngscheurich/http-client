
;--------------------------------------

;	.segment "zp"
	.comment
MakeChecksumZp_Ptr	.word 0
	.endc

;	.segment "data"
	.comment
MakeChecksumZp_Len	.word 0
MakeChecksumZp_Sum	.word 0
	.endc

;	.segment "bank0"

;--------------------------------------

MakeChecksum .proc
	ldx #0
	stx MakeChksum+1
        ldy #0

        clc
	lda MakeChecksumZp_Len+1
	beq MakeChecksum_Rest
-       jsr MakeChksum
	inc MakeChecksumZp_Ptr+1
	dec MakeChecksumZp_Len+1
	bne -

MakeChecksum_Rest
	lda MakeChecksumZp_Len
        beq null
        php
        eor #255
        tay
        eor #255
        clc
        adc MakeChecksumZp_Ptr
        sta MakeChecksumZp_Ptr
        bcs +
        dec MakeChecksumZp_Ptr+1
+       plp
        iny
	jsr MakeChksum
null    bcc MakeChksum3
        lda MakeChecksumZp_Len
        lsr
        bcs MakeChksum2

MakeChksum1
	inc MakeChksum+1
	bne MakeChksum3
MakeChksum2
	inx
	beq MakeChksum1

MakeChksum3

	txa
	eor #$ff
	tax
	lda MakeChksum+1
	eor #$ff
	rts

;--------------------------------------

MakeChksum
	lda #0
	adc (MakeChecksumZp_Ptr),y
	sta MakeChksum+1
	iny
	beq +

        txa
	adc (MakeChecksumZp_Ptr),y
	tax
	iny
	bne MakeChksum
+       rts
    .pend

;--------------------------------------

