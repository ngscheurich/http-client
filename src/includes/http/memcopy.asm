
;	.segment "zp"
.comment
CopyZp_Src		.word 0
CopyZp_Dst		.word 0
CopyZp_SrcEnd		.word 0
.endc
;--------------------------------------

;	.segment "data"
.comment
CopyZp_A		.byte 0
CopyZp_X		.byte 0
CopyZp_Y		.byte 0

CopyZp_Len		.word 0
.endc
;--------------------------------------

;	.segment "bank0"


MemCopy 	.proc
        	ldx CopyZp_Len+1
        	beq +
        	ldy #0

-		lda (CopyZp_Src),y		;kopiere ein byte
        	sta (CopyZp_Dst),y
        	dey
		bne -
		inc CopyZp_Src+1
		inc CopyZp_Dst+1
        	dex
        	bne -

+       	ldy CopyZp_Len
        	beq +
        	dey
        	beq e

-		lda (CopyZp_Src),y		;kopiere ein byte
        	sta (CopyZp_Dst),y
		dey
        	bne -

e      		lda (CopyZp_Src),y		;kopiere ein byte
        	sta (CopyZp_Dst),y

+       	rts
    		.pend

;--------------------------------------

