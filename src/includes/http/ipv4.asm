
;--------------------------------------

IpV4_Protocol_Tcp	= $06
IpV4_Protocol_Udp	= $11

;--------------------------------------

IpV4Header_Version	= $00
IpV4Header_DSF		= $01
IpV4Header_Length	= $02
IpV4Header_Id		= $04
IpV4Header_Flags	= $06
IpV4Header_TTL		= $08
IpV4Header_Protocol	= $09
IpV4Header_Checksum	= $0a
IpV4Header_SrcIp	= $0c
IpV4Header_DstIp	= $10
IpV4Header_length	= $14

;--------------------------------------

;	.segment "data"
	.comment
IpV4Zp_PacketLength		.word 0
IpV4Zp_Checksum			.word 0
IpV4Zp_BucketIn			.byte 0
IpV4Zp_BucketOut		.byte 0
IpV4Zp_Protocol			.byte 0
IpV4Zp_SrcIpIdx			.byte 0
IpV4Zp_DstIpIdx			.byte 0

IpV4Zp_TmpIp			*=*+4
	.endc
;	.segment "bank0"

;--------------------------------------
; Send IpV4 packet
; Parameters:
;  A : Protocol (TCP, UDP, ...)
;  X : Bucket Idx of Data packet
;  Y : ipCache Idx of Destination Ip
;
; Return:
;  Carry: Ok (Clear) / Error (Set)

ipv4_send 	.proc
		sta IpV4Zp_Protocol
		sty IpV4Zp_DstIpIdx
		; add length of ethernet header
		stx IpV4Zp_BucketIn

		; source ip is me
		lda MyIpIdx
		sta IpV4Zp_SrcIpIdx

		lda #IpV4Header_length
		jsr Bucket_GrowFront_small
		bcs error
		stx IpV4Zp_BucketOut

		jsr Bucket_Len
		sta IpV4Zp_PacketLength
		sty IpV4Zp_PacketLength+1

		jsr Bucket_GetAdr

		lda IpV4Zp_Protocol
		ldy #IpV4Header_Protocol
                gne +

copyFixed	lda fixedData,y
+		sta (BucketZp_BufPtr),y
       	 	dey
		bpl copyFixed

		;set src ip
		ldy IpV4Zp_SrcIpIdx
	        lda #IpV4Header_SrcIp
		jsr ipCache_Get_tobucket
		bcs error

		;set dst ip
		ldy IpV4Zp_DstIpIdx
		lda #IpV4Header_DstIp
		jsr ipCache_Get_tobucket
		bcs error

		; set Eth layer protocol
		lda #>Eth_Proto_Ip
		ldy #<Eth_Proto_Ip
		jsr eth_setProtocol

		ldy #IpV4Header_Length
		lda IpV4Zp_PacketLength+1
		sta (BucketZp_BufPtr),y
		iny
		lda IpV4Zp_PacketLength
		sta (BucketZp_BufPtr),y

		jsr ipv4_makeOutChecksum

		ldy IpV4Zp_DstIpIdx
		jsr arp_getDstMacAdr
		bcs error

		lda #>Eth_Proto_Ip
		ldy #<Eth_Proto_Ip
		jsr eth_setProtocol

		ldx IpV4Zp_BucketOut
		jmp eth_sendPacket

error
		sec
		rts

fixedData	.byte $45				;IpV4Header_Version
		.byte $00				;IpV4Header_DSF
		.byte $00,$00			;IpV4Header_Length
		.byte $00,$00			;IpV4Header_Id
		.byte $40,$00			;IpV4Header_Flags
		.byte $40				;IpV4Header_TTL
    		.pend


;--------------------------------------
; Get a ipv4 packet, this is called by the upper
; layer

ipv4_get = eth_getPacket

;--------------------------------------
; Process a received packet, this is called by
; the eth layer
ipv4_getPacket .proc
		stx IpV4Zp_BucketIn
		jsr Bucket_GetAdr

                ; no fragments
                ldy #IpV4Header_Flags
                lda (BucketZp_BufPtr),y
                and #$3f
                iny
                ora (BucketZp_BufPtr),y
                bne badPacket

		; by now we only know tcp
		ldy #IpV4Header_Protocol
		lda (BucketZp_BufPtr),y
		sta IpV4Zp_Protocol
		cmp #IpV4_Protocol_Udp
		beq knownProtocol
		cmp #IpV4_Protocol_Tcp
		bne badPacket

knownProtocol
		;target ip adr must be mine
		ldy #IpV4Header_DstIp
-		lda ActCfg_Ip-IpV4Header_DstIp,y
		cmp (BucketZp_BufPtr),y
		bne badPacket
		iny
                cpy #IpV4Header_DstIp+4
		blt -

		;test the packets checksum
		jsr ipv4_testInChecksum
		bcs badPacket

		;remember source ip
		lda BucketZp_BufPtr
		ldx BucketZp_BufPtr+1
		adc #IpV4Header_SrcIp
		bcc +
		inx
+		jsr ipCache_New
        	sty IpV4Zp_SrcIpIdx

		; everything ok, pass the packet to the upper layer
		ldy #IpV4Header_Length+1
		lda (BucketZp_BufPtr),y
		sta IpV4Zp_PacketLength
		dey
		lda (BucketZp_BufPtr),y
		sta IpV4Zp_PacketLength+1

		ldx IpV4Zp_BucketIn
		lda IpV4Zp_PacketLength
		ldy IpV4Zp_PacketLength+1
		jsr Bucket_ShrinkToLength
		bcs error

		ldy #IpV4Header_length
		jsr Bucket_ShrinkFront
		bcs error
		stx IpV4Zp_BucketOut

		ldy IpV4Zp_SrcIpIdx
		lda IpV4Zp_Protocol
		cmp #IpV4_Protocol_Udp
		beq udp_getPacket
		jmp tcp_getPacket

badPacket
error
		sec
		rts
    		.pend

;--------------------------------------

ipv4_makeOutChecksum .proc
		; clear checksum field
		ldy #IpV4Header_Checksum
		lda #0
		sta (BucketZp_BufPtr),y
		iny
		sta (BucketZp_BufPtr),y

		; start to make checksum at start of ip header
		lda BucketZp_BufPtr
		sta MakeChecksumZp_Ptr
		lda BucketZp_BufPtr+1
		sta MakeChecksumZp_Ptr+1

		; length of area to make checksum of is the ip header
		lda #<IpV4Header_length
		sta MakeChecksumZp_Len
		lda #>IpV4Header_length
		sta MakeChecksumZp_Len+1

		; calculate checksum for the ip header
		jsr MakeChecksum

		; store checksum in ipv4 header
		ldy #IpV4Header_Checksum
		sta (BucketZp_BufPtr),y
		iny
		txa
		sta (BucketZp_BufPtr),y

		rts
                .pend

;--------------------------------------

ipv4_testInChecksum .proc
		; save checksum to zp
		ldy #IpV4Header_Checksum
		lda (BucketZp_BufPtr),y
		sta IpV4Zp_Checksum+1
		iny
		lda (BucketZp_BufPtr),y
		sta IpV4Zp_Checksum

		jsr ipv4_makeOutChecksum

		ldy #IpV4Header_Checksum
		lda (BucketZp_BufPtr),y
		eor IpV4Zp_Checksum+1
		bne +
		iny
		lda (BucketZp_BufPtr),y
		eor IpV4Zp_Checksum
+               cmp #1
		rts
        	.pend

;--------------------------------------


