
;-------------------------------------
;basic.asm
;
fileName_len			*=*+1
fileName			*=*+256

MyMacAdr			*=*+6

MyIpIdx				*=*+1
MyNetMaskIdx			*=*+1
MyGatewayIdx			*=*+1
MyDnsIpIdx			*=*+1
DstIpIdx                        *=*+1

;-------------------------------------
;memcopy.asm
;

CopyZp_Len		*=*+2
;-------------------------------------
;buckets2.asm
;
Bucket_A		*=*+1
Bucket_X		*=*+1

;Bucket_SrcLen		*=*+2
Bucket_BestFitLen	*=*+2
Bucket_BestFitIdx	*=*+1
Bucket_Gap		*=*+2

Bucket_ShrinkIdx	*=*+1
Bucket_SizeDiff		*=*+2
Bucket_GrowIdx		*=*+1
Bucket_GrowNewIdx	*=*+1

Bucket_FirstEntry	*=*+1

Bucket_MaxAnzahl	= 32

; Pointer to the start of the memory block
Bucket_PtrLoTab		*=*+Bucket_MaxAnzahl
Bucket_PtrHiTab		*=*+Bucket_MaxAnzahl
; Length of the block
Bucket_LenLoTab		*=*+Bucket_MaxAnzahl
Bucket_LenHiTab		*=*+Bucket_MaxAnzahl
; Gap before the block
Bucket_GapLoTab		*=*+Bucket_MaxAnzahl
Bucket_GapHiTab		*=*+Bucket_MaxAnzahl
; Index to FastLookUp
Bucket_IdxTab		*=*+Bucket_MaxAnzahl

; index to Tabs
Bucket_FastLookUp	*=*+Bucket_MaxAnzahl
;-------------------------------------
;filename.asm
;
getBNumZp_Len			*=*+1
getBNumZp_Val			*=*+1
inputPos			*=*+1

HostNameLength			*=*+1
DnsZp_HostNameBuf		*=*+1

TempIp				*=*+4

getBNumBuf			*=*+3
;-------------------------------------
;ipCache.asm
;
IpCacheZp_A			*=*+1
IpCacheZp_X			*=*+1
IpCacheZp_Y			*=*+1
IpCache_Data			*=*+8*4	; room for 8 ip's
IpCache_Bam			*=*+1		; Buffer allocation map
;-------------------------------------
;timer.asm
;
timer_A				*=*+1
timer_BufNow			*=*+4
timer_BufMax			*=*+4
;-------------------------------------
;checksum.asm
;
MakeChecksumZp_Len	*=*+2
;-------------------------------------
;cs8900a.asm
;
CsZp_PacketLen			*=*+2
CsZp_Bucket			*=*+1

CsZp_TimeOutBuffer		*=*+4
;-------------------------------------
;phy.asm
;
PhyZp_MacAdr                    *=*+6

PhyJmp_Open			*=*+3
PhyJmp_Close			*=*+3
PhyJmp_GetPacket		*=*+3
PhyJmp_SendPacket		*=*+3

PhyZp_InitJmp			*=*+3

PhyZp_State			*=*+1
;-------------------------------------
;eth.asm
;
EthRec_SrcMac			*=*+6

Eth_DstMac			*=*+6
Eth_Proto			*=*+2

EthZp_TimeOutBuffer		*=*+4
;-------------------------------------
;ipv4.asm
;
IpV4Zp_PacketLength		*=*+2
IpV4Zp_Checksum			*=*+2
IpV4Zp_BucketIn			*=*+1
IpV4Zp_BucketOut		*=*+1
IpV4Zp_Protocol			*=*+1
IpV4Zp_SrcIpIdx			*=*+1
IpV4Zp_DstIpIdx			*=*+1

IpV4Zp_TmpIp			*=*+4
;-------------------------------------
;arp.asm
;
ArpZp_State			*=*+1
ArpZp_BucketIn			*=*+1
ArpZp_BucketOut			*=*+1

ArpZp_TmpIp			*=*+4

ArpZp_DstIp			*=*+4

ArpZp_DstIpIdx			*=*+1
ArpZp_ExpectedDstIpIdx		*=*+1

Arp_RemoteIP			*=*+4

Arp_CacheBam			*=*+1		; allocation map for the cache
Arp_CacheAge			*=*+8		; counts unused times, the oldest one will be replaced if the list is full
Arp_IpCache			*=*+8*4	; room for 8 ip's
Arp_MacCache			*=*+8*6	; room for 8 mac's

ArpZp_CacheCnt			*=*+1

ArpZp_Retries			*=*+1
ArpZp_TimeOutBuffer		*=*+4
;-------------------------------------
;udp.asm
;
UdpZp_Checksum			*=*+2

UdpZp_SrcPort			*=*+2
UdpZp_DstPort			*=*+2

UdpZp_BucketIn			*=*+1
UdpZp_BucketOut			*=*+1
UdpZp_BucketChecksumIn		*=*+1
UdpZp_BucketChecksumPsy		*=*+1

UdpZp_SrcIpIdx			*=*+1
UdpZp_DstIpIdx			*=*+1
UdpZp_ExpectedIpIdx		*=*+1

UdpZp_ChkSumPacketLength	*=*+2

UdpZp_TmpIp0			*=*+4
UdpZp_TmpIp1			*=*+4
;-------------------------------------
;tcp.asm
;
TcpZp_A				*=*+1
TcpZp_X				*=*+1
TcpZp_Y				*=*+1

TcpZp_State			*=*+1
TcpZp_Flags			*=*+1

TcpZp_BucketIn			*=*+1
TcpZp_BucketOut			*=*+1
TcpZp_BucketChecksumIn		*=*+1
TcpZp_BucketChecksumPsy	*=*+1
Tcp_ISN				*=*+4	; Initial Sequence Number
Tcp_SrcPort			*=*+2
Tcp_DstPort			*=*+2
                                .cerror Tcp_DstPort!=(Tcp_SrcPort+2)
Tcp_CSN				*=*+4	; Current Sequence Number
                                .cerror Tcp_CSN!=(Tcp_DstPort+2)
Tcp_ASN				*=*+4	; Acknowledged Sequence Number
                                .cerror Tcp_ASN!=(Tcp_CSN+4)
Tcp_PacketLength		*=*+2	; Length of the TCP Data part
Tcp_Stack			*=*+1
Tcp_StackSeqLo			*=*+1
Tcp_StackSeqHi			*=*+1
Tcp_StackSeqHi2			*=*+1

Tcp_GetSeq_First		*=*+4
Tcp_RecDataLength		*=*+3
Tcp_GetStackSeq			*=*+3

Tcp_RecDataBucket		*=*+1

TcpZp_GetBucket			*=*+1	;Bucket with valid input data, $ff means no bucket
TcpZp_GetLength			*=*+2	;Length of the GetBucket (to save the call to Bucket_Len for every get operation)

TcpZp_Checksum			*=*+2

TcpZp_ConnectedIpIdx		*=*+1
TcpZp_SrcIpIdx			*=*+1
TcpZp_DstIpIdx			*=*+1

TcpZp_Retries			*=*+1
TcpZp_TimeOutBuffer		*=*+4
;-------------------------------------
;http.asm
;
HttpZp_DataLength		*=*+2
HttpZp_State			*=*+1
HttpZp_BucketOut		*=*+1
Http_Line			*=*+256
HttpZp_LineLen			*=*+1
HttpZp_Num			*=*+2
HttpZp_NumLen			*=*+1
HttpZp_BlkLen			*=*+1
;-------------------------------------
;dns.asm
;
DnsZp_BucketIn			*=*+1
DnsZp_BucketOut			*=*+1

DnsZp_State			*=*+1

DnsZp_TransactionCnt		*=*+2

; Datablock for one request
DnsZp_Transaction		*=*+2
DnsZp_BucketRequest		*=*+1
DnsZp_RequestLen		*=*+1

DnsZp_Retries			*=*+1
DnsZp_TimeOutBuffer		*=*+4

dnsResult			*=*+4
;-------------------------------------
;settings.asm
;
Magic				*=*+6

ActCfg_Ip			.byte 192,168,0,3;10,11,14,64
ActCfg_Mac			.text "DREAMS"
ActCfg_NetMask			.byte 255,255,255,0
ActCfg_Router			.byte 192,168,0,1;10,11,14,250
ActCfg_Nameserver		.byte 10,1,1,1
;-------------------------------------
