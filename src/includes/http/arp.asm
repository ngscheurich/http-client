
;--------------------------------------

ArpCfg_Timeout		= 3
ArpCfg_RetryNum		= 5

;--------------------------------------

Arp_HardwareTyp		= $00
Arp_ProtocolTyp		= $02
Arp_HardwareSize	= $04
Arp_ProtocolSize	= $05
Arp_Opcode		= $06
Arp_SrcMacAdr		= $08
Arp_SrcIpAdr		= $0e
Arp_DstMacAdr		= $12
Arp_DstIpAdr		= $18
Arp_length		= $1c

;--------------------------------------

;	.segment "data"
	.comment
ArpZp_State			.byte 0
ArpZp_BucketIn			.byte 0
ArpZp_BucketOut			.byte 0

ArpZp_TmpIp			*=*+4

ArpZp_MyIp			*=*+4
ArpZp_DstIp			*=*+4
ArpZp_NetMask			*=*+4

ArpZp_DstIpIdx			.byte 0
ArpZp_ExpectedDstIpIdx		.byte 0

Arp_RemoteMac			*=*+6

Arp_CacheBam			.byte 0		; allocation map for the cache
Arp_CacheAge			*=*+8		; counts unused times, the oldest one will be replaced if the list is full
Arp_IpCache			*=*+8*4	; room for 8 ip's
Arp_MacCache			*=*+8*6	; room for 8 mac's

ArpZp_IpIdx			.byte 0
ArpZp_CacheCnt			.byte 0

ArpZp_Retries			.byte 0
ArpZp_TimeOutBuffer		*=*+4
	.endc
;	.segment "bank0"

;--------------------------------------
; statemachine for arp
; mainly used to determine is an arp reply packet was requested or not
; (to prevent mac overrides)

ARPSTATE_Idle			= 0
ARPSTATE_WaitForReply		= 1

;--------------------------------------

ARP_HwTyp_Ethernet		= $0001
ARP_Protocol_IP			= $0800

;--------------------------------------

ARP_Opcode_Request		= $0001
ARP_Opcode_Reply		= $0002

;--------------------------------------

arpCache_pot2Tab
	.byte $01,$02,$04,$08,$10,$20,$40,$80

;--------------------------------------

arp_init        .proc
		lda #ARPSTATE_Idle
		sta ArpZp_State

		lda #0
		sta Arp_CacheBam

		rts
                .pend

;--------------------------------------
; Cache routines

; Search the Arp_Ip Cache for the ip specified by the
; Ip index in y.
; The routine increases the CacheAge entry for all ip's,
; for speed reasons also the ones of unused entries. :)
; Returns: Carry clear = the ip was found in the cache and
;                        the according mac was copied to Arp_RemoteMac
;          Carry set   = the ip was not found

arpCache_request .proc
	lda Arp_CacheBam		; is the cache completely empty?
	beq error			; skip the rest, nothing found

	lda #<ArpZp_TmpIp		; get the ip data to the tmp buffer
	ldx #>ArpZp_TmpIp
	jsr ipCache_Get
	bcs error

	ldx #7
	stx ArpZp_CacheCnt		; compare IP caches 7 - 0

-       inc Arp_CacheAge,x              ; increase age
        bne +
        dec Arp_CacheAge,x              ; maximal age already reached -> don't inc anymore
+       dex				; next cache entry
	bpl -

cmpTmpIp0
	ldy ArpZp_CacheCnt		; is the bam bit for this entry set -> the entry is valid
	lda arpCache_pot2Tab,y
	and Arp_CacheBam
	beq cmpTmpIp2			; entry is not valid -> skip

	tya			  	; get index for ip cache
	asl
	asl
        ora #3
	tay
	ldx #3
-       lda ArpZp_TmpIp,x
	cmp Arp_IpCache,y
	bne cmpTmpIp2
        dey
	dex
	bpl -

	ldx ArpZp_CacheCnt		; entry was used, reset age to 0
	lda #0
	sta Arp_CacheAge,x

	txa				; mulitply ArpZp_CacheCnt with 6
	asl				; *2
	sta ArpZp_CacheCnt
	asl				; *4
	adc ArpZp_CacheCnt		; *4 + *2 = *6
	tay

	ldx #250
-	lda Arp_MacCache,y
	sta Eth_DstMac-250,x
	iny
	inx
	bne -

	clc
	rts

cmpTmpIp2
	dec ArpZp_CacheCnt		; compare next cache entry
	bpl cmpTmpIp0
error
	sec				; no more left -> IP not found in cache
	rts
    .pend


arpCache_enter .proc
	ldy ArpZp_ExpectedDstIpIdx
	lda #<ArpZp_TmpIp
	ldx #>ArpZp_TmpIp
	jsr ipCache_Get
	bcs error

	lda Arp_CacheBam		; is the cache completely full?
	cmp #$ff
	beq replaceEntry

	ldx #$ff			; get the number of the first free entry
findFreeEntry
	inx
	lsr
	bcs findFreeEntry
	stx ArpZp_CacheCnt		; remember index of the free entry
	bcc makeEntry

	; all entries are occupied, replace the
	; oldest one (the highest Arp_CacheAge)
replaceEntry
	lda #0				; max age, start with the minimum of 0
	ldx #7				; check entries 0-7
	stx ArpZp_CacheCnt		; index of the oldest entry, start with the last one
replaceEntry0
	cmp Arp_CacheAge,x		; maxAge-entryAge>=0 <-> maxAge>=entryAge <-> no new maxima
	bcs replaceEntry1
	lda Arp_CacheAge,x		; get new maxAge
	stx ArpZp_CacheCnt		; remember index of the oldest entry
replaceEntry1
	dex				; check next entry
	bpl replaceEntry0
	ldx ArpZp_CacheCnt		; get the index of the oldest entry

makeEntry
	lda Arp_CacheBam		; allocate entry
	ora arpCache_pot2Tab,x
	sta Arp_CacheBam

	lda #0				; age of new entry is 0
	sta Arp_CacheAge,x

	txa
	asl
	asl
	tay
	ldx #0
copyIp
	lda ArpZp_TmpIp,x
	sta Arp_IpCache,y
	iny
	inx
	cpx #4
	bcc copyIp

	lda ArpZp_CacheCnt		; mulitply ArpZp_CacheCnt with 6
	asl				; *2
	sta ArpZp_CacheCnt
	asl				; *4
	adc ArpZp_CacheCnt		; *4 + *2 = *6
	tay

	ldx #250
copyRemoteMac
	lda Eth_DstMac-250,x
	sta Arp_MacCache,y
	iny
	inx
	bne copyRemoteMac

	clc
	rts

error
	sec
	rts
    .pend

;--------------------------------------

arp_getDstMacAdr .proc

	sty ArpZp_DstIpIdx

	;select the destination mac depending on router cfg
	;if (MyIpAdr AND MyNetMask)==(DstIpAdr AND MyNet)

	lda #<ArpZp_DstIp
	ldx #>ArpZp_DstIp
	jsr ipCache_Get
	bcs error

        ldy MyGatewayIdx
	ldx #3
-	lda ActCfg_Ip,x
	eor ArpZp_DstIp,x
	and ActCfg_NetMask,x
        bne viaGateway
	dex
	bpl -
	; send directly to destination
	ldy ArpZp_DstIpIdx
viaGateway
	sty ArpZp_DstIpIdx

	jsr arpCache_request
	bcc macFound

	lda #ArpCfg_RetryNum
	sta ArpZp_Retries
	lda #ARPSTATE_WaitForReply
	sta ArpZp_State
resendReq
	ldy ArpZp_DstIpIdx
	jsr arp_sendRequest
	bcs error

	ldx #<ArpZp_TimeOutBuffer
	ldy #>ArpZp_TimeOutBuffer
	jsr timer_get

getPacket
	jsr eth_getPacket
	bcs notFound

	; check state to see if an arp packet was received
	lda ArpZp_State
	cmp #ARPSTATE_Idle
	beq macFound

notFound
	lda #ArpCfg_Timeout
	ldx #<ArpZp_TimeOutBuffer
	ldy #>ArpZp_TimeOutBuffer
	jsr timer_check
	bcc getPacket

	dec ArpZp_Retries
	bne resendReq

error
	sec
	rts

macFound
	clc
	rts
    .pend

;--------------------------------------

arp_getPacket .proc
	stx ArpZp_BucketIn
	jsr Bucket_GetAdr
	ldy #Arp_Opcode
	lda (BucketZp_BufPtr),y		; all ARP Opcodes start with $00..
	bne strangePacket		; no $00.. -> weird packet biased
	iny
	lda (BucketZp_BufPtr),y
	cmp #<ARP_Opcode_Request
	beq arp_getRequest
	cmp #<ARP_Opcode_Reply
	beq arp_getReply

	; ... more to come (???)
strangePacket
	jmp eth_getPacket		; strange things received, ignore

    .pend

;--------------------------------------
; arp reply

arp_getReply .proc
	lda ArpZp_State
	cmp #ARPSTATE_WaitForReply
	bne eth_getPacket			;we are not waiting for a reply packet -> ignore it

	jsr arp_testParams
	bcs strange_packet

	ldy ArpZp_ExpectedDstIpIdx
	lda #<ArpZp_TmpIp
	ldx #>ArpZp_TmpIp
	jsr ipCache_Get
	bcs error

	;source ip adr must be the expected ip
	ldy #Arp_SrcIpAdr
-	lda (BucketZp_BufPtr),y
	cmp ArpZp_TmpIp-Arp_SrcIpAdr,y
	bne strange_packet
	iny
        cpy #Arp_SrcIpAdr+4
	blt -

	;target mac adr must be mine
	ldy #Arp_DstMacAdr
-	lda (BucketZp_BufPtr),y
	cmp MyMacAdr-Arp_DstMacAdr,y
	bne strange_packet
	iny
        cpy #Arp_DstMacAdr+6
	blt -

	;TODO: Received Eth2 Layer Mac must be the Gateway or equal to Arp_SrcMacAdr

	;ok, all is fine. take the mac adr as the dst
	ldy #Arp_SrcMacAdr
-	lda (BucketZp_BufPtr),y
	sta Eth_DstMac-Arp_SrcMacAdr,y
	iny
        cpy #Arp_SrcMacAdr+6
	blt -

	; enter ip and mac into the arp cache
	jsr arpCache_enter
	bcs error

	ldx ArpZp_BucketIn
	jsr Bucket_Free

	lda #ARPSTATE_Idle
	sta ArpZp_State

	clc
	rts

strange_packet
	jmp eth_getPacket			; ignore strange packet

error
	sec
	rts

    .pend

;--------------------------------------
; arp request received
; reply if the ip is matching

arp_getRequest .proc
	; yay, a request. but is it really for me?
	jsr arp_testParams
	bcc fixedParamsOk

strange_packet
	; ignore packet, wait for next one
	ldx ArpZp_BucketIn
	jsr Bucket_Free

	jmp eth_getPacket

bucketError
error
	sec
	rts

fixedParamsOk
	ldy #Arp_DstMacAdr
-	lda (BucketZp_BufPtr),y
	bne strange_packet			; it's not looking for the mac adr. is this senseless or am I missing something
	lda MyMacAdr-Arp_DstMacAdr,y
	sta (BucketZp_BufPtr),y
	iny
        cpy #Arp_DstMacAdr+6
	blt -

	; everything was ok, send a reply packet

	; reuse old packet for reply

	; copy Mac Address and IP of the request package
	; this is the destination mac/ip for the reply
	ldy #Arp_SrcMacAdr
-	lda (BucketZp_BufPtr),y
	pha
	iny
        cpy #Arp_SrcMacAdr+20
	blt -

	; copy src Mac and IP to Dst Mac and Ip
	ldy #Arp_SrcMacAdr+9
-	pla
        sta (BucketZp_BufPtr),y
	dey
        cpy #Arp_SrcMacAdr
	bge -

	ldy #Arp_DstMacAdr+9
-	pla
        sta (BucketZp_BufPtr),y
	dey
        cpy #Arp_DstMacAdr
	bge -

        ; set Eth Mac Address
        ldy #Arp_DstMacAdr
-       lda (BucketZp_BufPtr),y
        sta Eth_DstMac-Arp_DstMacAdr,y
        iny
        cpy #Arp_DstMacAdr+6
        blt -

        ldy #Arp_Opcode+1
        lda #<ARP_Opcode_Reply
	sta (BucketZp_BufPtr),y

	; set Eth layer protocol
	lda #>Eth_Proto_Arp
	ldy #<Eth_Proto_Arp
	jsr eth_setProtocol

	ldx ArpZp_BucketIn
	jsr eth_sendPacket

	; wait for next packet
	jmp eth_getPacket

    .pend

;--------------------------------------

arp_testParams .proc
	;is it a correct ARP packet?
	;hw_type = ethernet, protocol=ip, hw_size=4, proto_size=4
	ldy #Arp_Opcode-1
-	lda (BucketZp_BufPtr),y
	cmp arpfixedData,y
	bne nogood
	dey
	bpl -

	;target ip adr must be mine
	ldy #Arp_DstIpAdr
-	lda (BucketZp_BufPtr),y
	cmp ActCfg_Ip-Arp_DstIpAdr,y
	bne nogood
	iny
        cpy #Arp_DstIpAdr+4
	blt -

	clc
	rts

nogood  sec
	rts
    .pend

;--------------------------------------

arp_sendRequest .proc
	sty ArpZp_ExpectedDstIpIdx

	lda #<Arp_length
	ldy #>Arp_length
	jsr Bucket_Alloc
	bcs bucketError
	stx ArpZp_BucketOut

	; first 8 bytes are fixed for an arp request
	ldy #Arp_SrcMacAdr-1
-	lda arpfixedData,y
	sta (BucketZp_BufPtr),y
	dey
	bpl -

	; Copy the Source Mac Adr
	ldy #Arp_SrcMacAdr
-	lda MyMacAdr-Arp_SrcMacAdr,y
	sta (BucketZp_BufPtr),y
	iny
        cpy #Arp_SrcMacAdr+6
	blt -

	ldy #Arp_SrcIpAdr
-	lda ActCfg_Ip-Arp_SrcIpAdr,y
	sta (BucketZp_BufPtr),y
	iny
        cpy #Arp_SrcIpAdr+4
	blt -

	; Clear the Dst Mac Adr, this is the info to request
	ldy #Arp_DstMacAdr
-       lda #0
	sta (BucketZp_BufPtr),y
        lda #$ff
	sta Eth_DstMac-Arp_DstMacAdr,y  ; set Eth Mac Address
	iny
        cpy #Arp_DstMacAdr+6
	blt -

	; Copy the Dst IP Adr
	ldy ArpZp_ExpectedDstIpIdx
	lda BucketZp_BufPtr
	ldx BucketZp_BufPtr+1
	clc
	adc #Arp_DstIpAdr
	bcc +
	inx
+	jsr ipCache_Get
	bcs error

	; set Eth layer protocol
	lda #>Eth_Proto_Arp
	ldy #<Eth_Proto_Arp
	jsr eth_setProtocol

	ldx ArpZp_BucketOut
	jmp eth_sendPacket

error
bucketError
	sec
	rts
    .pend

arpfixedData
	.byte >ARP_HwTyp_Ethernet, <ARP_HwTyp_Ethernet
	.byte >ARP_Protocol_IP, <ARP_Protocol_IP
	.byte 6
	.byte 4
	.byte >ARP_Opcode_Request, <ARP_Opcode_Request

;--------------------------------------
