	.logical $57
;-------------------------------
;memcopy.asm
;
CopyZp_Src              .fill 2
CopyZp_Dst              .fill 2
;-------------------------------
;buckets2.asm
;
BucketZp_BufPtr         .fill 2
bucket_tmp1             .fill 1
bucket_tmp2             .fill 1
bucket_tmp3             .fill 1
bucket_tmp4             .fill 1
;-------------------------------
;ipCache.asm
;
IpCacheZp_Ptr                   .fill 2
;-------------------------------
;timer.asm
;
timerZp_Ptr                     .fill 2
;-------------------------------
;checksum.asm
;
MakeChecksumZp_Ptr      .fill 2
;-------------------------------
        .here