
ipCache_pot2Tab
		.byte $01,$02,$04,$08,$10,$20,$40,$80

ipCache_Init	.proc
		lda #0
		sta IpCache_Bam
error		rts
		.pend

ipCache_New	.proc
		sta IpCacheZp_Ptr
		lda IpCache_Bam
		cmp #$ff
		beq ipCache_Init.error

		ldy #$ff
-		iny
		lsr
		bcs -

		lda IpCache_Bam
		ora ipCache_pot2Tab,y
		sta IpCache_Bam
		sty IpCacheZp_Y

		gcc ipCache_Get.in
		.pend

ipCache_Get_tobucket .proc
		ldx BucketZp_BufPtr+1
		clc
		adc BucketZp_BufPtr
		bcc ipCache_Get
		inx
		gcs ipCache_Get
		.pend

ipCache_Get	.proc
		sec
		sty IpCacheZp_Y
		sta IpCacheZp_Ptr

	; is field allocated?
		lda IpCache_Bam
		and ipCache_pot2Tab,y
		beq error

in		stx IpCacheZp_Ptr+1

		php
		iny
		tya
		asl
		asl
		tax
		plp

		ldy #3
		bcs get
-		lda (IpCacheZp_Ptr),y
		dex
		sta IpCache_Data,x
		dey
		bpl -
		gmi put

get		clc
		ldy #3
-		dex
		lda IpCache_Data,x
		sta (IpCacheZp_Ptr),y
		dey
		bpl -
put
		lda IpCacheZp_Ptr
		ldx IpCacheZp_Ptr+1
		ldy IpCacheZp_Y
error
		rts
		.pend

;-------------------------
;Free entry (Y)
;
ipCache_Free	.proc
		lda ipCache_pot2Tab,y
		eor #$ff
		and IpCache_Bam
		sta IpCache_Bam
		rts
		.pend

