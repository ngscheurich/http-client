
;--------------------------------------

;	.segment "bank1"


;--------------------------------------

;ShowErrorMessage
;	ldx Status
;	lda MessagesLo,x
;	ldy MessagesHi,x
;	jsr $ab1e
;	lda #$0d
;	jmp $ffd2
;
;--------------------------------------

;MessagesLo
;	.byte <Error_Ok_Msg
;	.byte <Error_NoCS8900aMagicFound_Msg
;	.byte <Error_UnknownCS8900aVersionNumber_Msg
;	.byte <Error_Cfg_Ip_Msg
;	.byte <Error_MissingComma_Msg
;	.byte <Error_MissingDot_Msg
;	.byte <Error_MissingSlash_Msg
;	.byte <Error_OutOfBucketMemory_Msg
;	.byte <Error_IllegalBucketIndex_Msg
;	.byte <Error_Timeout_Msg
;	.byte <Error_TcpConnectionRefused_Msg
;	.byte <Error_LinkDown
;	.byte <Error_Phy_OpenDriver_Msg
;	.byte <Error_Phy_UnknownDriverIdx_Msg
;	.byte <Error_Phy_IllegalOperationInState_Msg

;MessagesHi
;	.byte >Error_Ok_Msg
;	.byte >Error_NoCS8900aMagicFound_Msg
;	.byte >Error_UnknownCS8900aVersionNumber_Msg
;	.byte >Error_Cfg_Ip_Msg
;	.byte >Error_MissingComma_Msg
;	.byte >Error_MissingDot_Msg
;	.byte >Error_MissingSlash_Msg
;	.byte >Error_OutOfBucketMemory_Msg
;	.byte >Error_IllegalBucketIndex_Msg
;	.byte >Error_Timeout_Msg
;	.byte >Error_TcpConnectionRefused_Msg
;	.byte >Error_LinkDown_Msg
;	.byte >Error_Phy_OpenDriver_Msg
;	.byte >Error_Phy_UnknownDriverIdx_Msg
;	.byte >Error_Phy_IllegalOperationInState_Msg

;--------------------------------------

;Error_Ok_Msg
;	.text "OK",0

;Error_NoCS8900aMagicFound_Msg
;	.text "NO RRNET FOUND!",$0d,0

;Error_UnknownCS8900aVersionNumber_Msg
;	.text "UNKNOWN CRYSTAL VERSION FOUND!",0

;Error_Cfg_Ip_Msg
;	.text "SYNTAX ERROR IN IP ADDRESS!",0

;Error_MissingComma_Msg
;	.text "MISSING ','",0

;Error_MissingDot_Msg
;	.text "MISSING '.'",0

;Error_MissingSlash_Msg
;	.text "MISSING '/'",0

;Error_OutOfBucketMemory_Msg
;	.text "OUT OF BUCKET MEMORY",0

;Error_IllegalBucketIndex_Msg
;	.text "ILLEGAL BUCKET INDEX",0

;Error_Timeout_Msg
;	.text "TIMEOUT",0

;Error_TcpConnectionRefused_Msg
;	.text "TCP CONNECTION REFUSED",0

;Error_LinkDown_Msg
;	.text "THE PHYSICAL LINK IS DOWN",0

;Error_Phy_OpenDriver_Msg
;	.text "UNABLE TO OPEN THE PHYSICAL DEVICE",0

;Error_Phy_UnknownDriverIdx_Msg
;	.text "UNKNOWN PHYSICAL DRIVER",0

;Error_Phy_IllegalOperationInState_Msg
;	.text "PHYSICAL LAYER OPERATION NOT PERMITTED IN THIS STATE",0

;--------------------------------------



