
;--------------------------------------
;Buckets
;
;Bucket_Init
;    Init the bucket system
;  Parameters
;    -
;  Return
;    -
;
;Bucket_Alloc
;    allocates a bucket and sets BucketZp_BufPtr to it's address
;  Parameter
;    Akku  = Lo Byte der geforderten Groesse des Speicherbereichs
;    Y     = Hi Byte der geforderten Groesse des Speicherbereichs
;  Return
;    Carry = ok (clear), error (set)
;    X     = (only if ok) number of the allocated bucket
;
;
;Bucket_Len
;    gibt die Groesse eines Eimers zurueck
;  Parameter
;    X     = Nummer des Eimers
;  Return
;    Carry = Erfolg (clear), Fehler (set)
;    Akku  = (nur bei Erfolg) Lo Byte der Groesse des Eimers
;    Y     = (nur bei Erfolg) Hi Byte der Groesse des Eimers
;
;
;Bucket_GetAdr
;    Holt Adresse eines Eimers
;  Parameter
;    X     = Nummer des Eimers
;  Return
;    Carry = Erfolg (clear), Fehler (set)
;    Akku  = (nur bei Erfolg) Lo Byte der Adresse des Eimers
;    Y     = (nur bei Erfolg) Hi Byte der Adresse des Eimers
;
;
;Bucket_Resize
;    Veraendert Groesse des Eimers
;  Parameter
;    Akku  = Lo Byte der Groesse des Eimers
;    X     = Nummer des Eimers
;    Y     = Hi Byte der Groesse des Eimers
;  Return
;    Carry = Erfolg (clear), Fehler (set)
;
;
;Bucket_Free
;    Gibt einen mit Bucket_Alloc reservierten Eimer wieder frei
;  Parameter
;    X     = Nummer des Eimer
;  Return
;    Carry = Erfolg (clear), Fehler (set)
;
;--------------------------------------

; In some routines I depend on Bucket_MaxAnzahl<$80, so be sure to keep it
; below.

Bucket_MemEnd		= Bucket_MemStart+Bucket_MemLen

;--------------------------------------


Bucket_Init	.proc

	lda #Bucket_MaxAnzahl-1		; one pseudo entry yet (zero length, all gap)
	sta Bucket_FirstEntry

					; fill pseudo entry
	lda #0				; length is 0
	sta Bucket_LenLoTab+Bucket_MaxAnzahl-1
	sta Bucket_LenHiTab+Bucket_MaxAnzahl-1
					; ptr is end of mem
	lda #<Bucket_MemEnd
	sta Bucket_PtrLoTab+Bucket_MaxAnzahl-1
	lda #>Bucket_MemEnd
	sta Bucket_PtrHiTab+Bucket_MaxAnzahl-1

	lda #<Bucket_MemLen		; gap is the complete memory
	sta Bucket_GapLoTab+Bucket_MaxAnzahl-1
	lda #>Bucket_MemLen
	sta Bucket_GapHiTab+Bucket_MaxAnzahl-1

	lda #$ff			; no idx
	sta Bucket_IdxTab+Bucket_MaxAnzahl-1
	ldx #Bucket_MaxAnzahl-1
initFastLookup
	sta Bucket_FastLookUp,x
	dex
	bpl initFastLookup
	rts

    .pend


;--------------------------------------

Bucket_Alloc .proc

Bucket_SrcLen = bucket_tmp1

	; save registers
	sta Bucket_SrcLen	; requested length lo
	sty Bucket_SrcLen+1	; requested length hi

	; TODO: return false for 0 length

	; is the list full?
	ldx Bucket_FirstEntry
	bne searchGap

	; no listentry -> can't alloc mem
;	lda #SomeErrorcode
;	sta MyStatus
error
	sec
	rts

searchGap
	; first try the start gap (first element)
	sec
	lda Bucket_GapLoTab,x
	sbc Bucket_SrcLen
	tay
	lda Bucket_GapHiTab,x
	sbc Bucket_SrcLen+1
	bcs insertEntry

	; does not fit into first gap ->
	; look for best fitting gap in the rest of the list
	ldy #$ff
	sty Bucket_BestFitLen
	sty Bucket_BestFitLen+1
	sty Bucket_BestFitIdx

searchLoop
	inx
	cpx #Bucket_MaxAnzahl
	bcs endOfList

	; does the requested length fit into the gap?
	sec
	lda Bucket_GapLoTab,x
	sbc Bucket_SrcLen
	tay
	lda Bucket_GapHiTab,x
	sbc Bucket_SrcLen+1
	bcc searchLoop

	; save gapsize
;	sty Bucket_Gap
	sta Bucket_Gap+1

	; did we find a better gap already?
	cpy Bucket_BestFitLen
	sbc Bucket_BestFitLen+1
	bcs searchLoop

	; no -> remember new values
	sty Bucket_BestFitLen
	lda Bucket_Gap+1
	sta Bucket_BestFitLen+1
	stx Bucket_BestFitIdx
	bcc searchLoop	;bra

endOfList
	ldx Bucket_BestFitIdx
	cpx #$ff
	bne gapFound

;	lda #SomeErrorcode
;	sta MyStatus
	sec
	rts

gapFound
	; move table entries down
	ldx Bucket_FirstEntry
	dex
moveTabsDown
	lda Bucket_PtrLoTab+1,x
	sta Bucket_PtrLoTab,x
	lda Bucket_PtrHiTab+1,x
	sta Bucket_PtrHiTab,x
	lda Bucket_LenLoTab+1,x
	sta Bucket_LenLoTab,x
	lda Bucket_LenHiTab+1,x
	sta Bucket_LenHiTab,x
	lda Bucket_GapLoTab+1,x
	sta Bucket_GapLoTab,x
	lda Bucket_GapHiTab+1,x
	sta Bucket_GapHiTab,x
	lda Bucket_IdxTab+1,x
	sta Bucket_IdxTab,x
	tay
	txa
	sta Bucket_FastLookUp,y
	inx
	cpx Bucket_BestFitIdx
	bcc moveTabsDown

insertEntry
	dec Bucket_FirstEntry

	; add entry before x
	dex
	stx Bucket_BestFitIdx

	; set length
	lda Bucket_SrcLen
	sta Bucket_LenLoTab,x
	lda Bucket_SrcLen+1
	sta Bucket_LenHiTab,x

	; pregap is nextPregap-Len
	lda Bucket_GapLoTab+1,x
	sec
	sbc Bucket_SrcLen
	sta Bucket_GapLoTab,x
	lda Bucket_GapHiTab+1,x
	sbc Bucket_SrcLen+1
	sta Bucket_GapHiTab,x

	; pregap for next entry is 0
	lda #0
	sta Bucket_GapLoTab+1,x
	sta Bucket_GapHiTab+1,x

	; start is nextStart-Len
	lda Bucket_PtrLoTab+1,x
	sec
	sbc Bucket_SrcLen
	sta Bucket_PtrLoTab,x
	sta BucketZp_BufPtr
	lda Bucket_PtrHiTab+1,x
	sbc Bucket_SrcLen+1
	sta Bucket_PtrHiTab,x
	sta BucketZp_BufPtr+1

	; make fast lookup entry
	lda #$ff
	ldy #Bucket_MaxAnzahl
searchFreeIdx
	dey
	cmp Bucket_FastLookUp,y
	bne searchFreeIdx

	txa
	sta Bucket_FastLookUp,y
	tya
	sta Bucket_IdxTab,x
	tax

	lda Bucket_SrcLen
	ldy Bucket_SrcLen+1
	clc
	rts
    .pend


;--------------------------------------

Bucket_Len .proc
Bucket_X = bucket_tmp1

	stx Bucket_X

	lda Bucket_FastLookUp,x
	tax
	lda Bucket_LenLoTab,x
	ldy Bucket_LenHiTab,x

	ldx Bucket_X
	rts

    .pend

;--------------------------------------

Bucket_GetAdr .proc
Bucket_X = bucket_tmp1

	stx Bucket_X

	lda Bucket_FastLookUp,x
	tax
	lda Bucket_PtrLoTab,x
	ldy Bucket_PtrHiTab,x
	sta BucketZp_BufPtr
	sty BucketZp_BufPtr+1

	ldx Bucket_X
	rts
    .pend

;--------------------------------------

Bucket_Free .proc
Bucket_SrcLen = bucket_tmp1
Bucket_X = bucket_tmp3
Bucket_Y = bucket_tmp4

	sta Bucket_A
	stx Bucket_X
	sty Bucket_Y

	lda Bucket_FastLookUp,x
	bmi error

	tax

	; add pregap and length to next pregap
	lda Bucket_GapLoTab,x
	clc
	adc Bucket_LenLoTab,x
	sta Bucket_SrcLen
	lda Bucket_GapHiTab,x
	adc Bucket_LenHiTab,x
	sta Bucket_SrcLen+1

	; add freed memory and gap to next entry
	lda Bucket_SrcLen
	clc
	adc Bucket_GapLoTab+1,x
	sta Bucket_GapLoTab+1,x
	lda Bucket_SrcLen+1
	adc Bucket_GapHiTab+1,x
	sta Bucket_GapHiTab+1,x

	; clear FastLookup entry
	ldy Bucket_X
	lda #$ff
	sta Bucket_FastLookUp,y

	; no need to move the list if the first entry was deleted
	cpx Bucket_FirstEntry
	beq free0

	; move entries up
moveTabsUp
	lda Bucket_PtrLoTab-1,x
	sta Bucket_PtrLoTab,x
	lda Bucket_PtrHiTab-1,x
	sta Bucket_PtrHiTab,x
	lda Bucket_LenLoTab-1,x
	sta Bucket_LenLoTab,x
	lda Bucket_LenHiTab-1,x
	sta Bucket_LenHiTab,x
	lda Bucket_GapLoTab-1,x
	sta Bucket_GapLoTab,x
	lda Bucket_GapHiTab-1,x
	sta Bucket_GapHiTab,x
	lda Bucket_IdxTab-1,x
	sta Bucket_IdxTab,x
	tay
	txa
	sta Bucket_FastLookUp,y
	dex
	cpx Bucket_FirstEntry
	bcs moveTabsUp

free0
	inc Bucket_FirstEntry

	lda Bucket_A
	ldx Bucket_X
	ldy Bucket_Y
	clc
error	rts

    .pend


;--------------------------------------
;Adjustment in Y
;Bucket in X
;

Bucket_ShrinkFront .proc
Bucket_ShrinkIdx = bucket_tmp1

	stx Bucket_ShrinkIdx

	lda Bucket_FastLookUp,x
	bmi error

	tax

        tya
        eor #255
        sec
        adc Bucket_LenLoTab,x
        bcs +
	dec Bucket_LenHiTab,x
        bmi error2
+	sta Bucket_LenLoTab,x

	; add sizeDiff to preGap
	clc
	tya
	adc Bucket_GapLoTab,x
	sta Bucket_GapLoTab,x
        bcc +
	inc Bucket_GapHiTab,x
+
	; add sizeDiff to ptr
        clc
	tya
	adc Bucket_PtrLoTab,x
	sta Bucket_PtrLoTab,x
        bcc +
	inc Bucket_PtrHiTab,x
+
	ldx Bucket_ShrinkIdx
	clc
        rts

error2  inc Bucket_LenHiTab,x
error	sec
	rts
    .pend

;--------------------------------------

Bucket_ShrinkToLength .proc
Bucket_SrcLen = bucket_tmp1
Bucket_SizeDiff = bucket_tmp3

	sta Bucket_SrcLen
	stx Bucket_ShrinkIdx
	sty Bucket_SrcLen+1

	lda Bucket_FastLookUp,x
	cmp #$ff
	beq error

	tax

	sec
	lda Bucket_LenLoTab,x
	sbc Bucket_SrcLen
	sta Bucket_SizeDiff
	lda Bucket_LenHiTab,x
	sbc Bucket_SrcLen+1
	sta Bucket_SizeDiff+1
	bcc error		; can't shrink more than the original size!

	; add sizeDiff to next preGap
	clc
	lda Bucket_GapLoTab+1,x
	adc Bucket_SizeDiff
	sta Bucket_GapLoTab+1,x
	lda Bucket_GapHiTab+1,x
	adc Bucket_SizeDiff+1
	sta Bucket_GapHiTab+1,x

	; set new length
	lda Bucket_SrcLen
	sta Bucket_LenLoTab,x
	lda Bucket_SrcLen+1
	sta Bucket_LenHiTab,x

	lda Bucket_SrcLen
	ldx Bucket_ShrinkIdx
	ldy Bucket_SrcLen+1
	clc
	rts

error	brk
;	sec
;	rts
    .pend

;--------------------------------------

Bucket_GrowFront_small .proc
Bucket_SrcLen = bucket_tmp1

	sta Bucket_SizeDiff
	stx Bucket_GrowIdx

	lda Bucket_FastLookUp,x
	cmp #$ff
	bcs error

	tax

        ;clc
	lda Bucket_LenLoTab,x
	adc Bucket_SizeDiff
	sta Bucket_SrcLen
	lda Bucket_LenHiTab,x
	adc #0
	sta Bucket_SrcLen+1

	; first try to fit the additional mem into the pregap, so no mem must
 	; be moved
	sec
	lda Bucket_GapLoTab,x
	sbc Bucket_SizeDiff
	tay
	lda Bucket_GapHiTab,x
	sbc #0
	bcc move

	sta Bucket_GapHiTab,x
	tya
	sta Bucket_GapLoTab,x

	; sub SizeDiff from Pointer
	;sec
	lda Bucket_PtrLoTab,x
	sbc Bucket_SizeDiff
	sta Bucket_PtrLoTab,x
        bcs +
	dec Bucket_PtrHiTab,x
+
	; update length
	lda Bucket_SrcLen
	sta Bucket_LenLoTab,x
	lda Bucket_SrcLen+1
	sta Bucket_LenHiTab,x

	lda Bucket_IdxTab,x
	sta Bucket_GrowNewIdx
	jmp end	;bra


error	brk
;	sec
;	rts


	; does not fit without moving data,
move
	; set source ptr and length
	lda Bucket_PtrLoTab,x
	sta CopyZp_Src
	lda Bucket_PtrHiTab,x
	sta CopyZp_Src+1
	lda Bucket_LenLoTab,x
	sta CopyZp_Len
	lda Bucket_LenHiTab,x
	sta CopyZp_Len+1

	; allocate a new buffer
	lda Bucket_SrcLen
	ldy Bucket_SrcLen+1
	jsr Bucket_Alloc
	bcs error
	stx Bucket_GrowNewIdx

	; copy old buffer to the end of the new buffer
	;clc
	lda BucketZp_BufPtr
	adc Bucket_SizeDiff
	sta CopyZp_Dst
	lda BucketZp_BufPtr+1
	adc #0
	sta CopyZp_Dst+1

	jsr MemCopy

	; free the old location
	ldx Bucket_GrowIdx
	jsr Bucket_Free
;	bcs error

end

	lda Bucket_SizeDiff
	ldx Bucket_GrowNewIdx
	clc
	rts
    .pend

;--------------------------------------

