;-----------------------------
;settings.asm
;
SelectedInputItem
InputZp_ScrPtr			*=*+2	; edit buffer on the screen
InputZp_EditDataPtr		*=*+2	; edit value
InputZp_ActDataPtr		*=*+2	; act value
InputZp_DefaultDataPtr		*=*+2	; default value
InputZp_Typ			*=*+1
InputZp_LenM1			*=*+1	; Length minus 1, must be >=$00 and <InputCfg_MaxLength
InputZp_Neighbors		*=*+4
InputZp_HelpText		*=*+2

InputZp_Pos			*=*+1
InputZp_Len			*=*+1	; Length

InputZp_ActItem			*=*+1

InputZp_ButtonBlinkCnt		*=*+1
InputZp_ButtonBlinkFlag		*=*+1


showMacZp_DataPtr		*=*+2
showMacZp_ScrPtr		*=*+2
showMacZp_Col			*=*+1

showHelpZp_TxtPtr		= showMacZp_DataPtr	; word
showHelpZp_ScrPtr		= showMacZp_DataPtr+2	; word

;shared locations with showHelp:
getDezNumZp_Len			= showMacZp_DataPtr	; byte
getDezNumZp_Val			= showMacZp_DataPtr+1	; byte

;shared locations with showHelp:
setLeaveColorZp_Col		= showMacZp_DataPtr	; byte
setLeaveColorZp_Ebm		= showMacZp_DataPtr+1	; byte

;shared locations with showHelp:
setButtonColorZp_Col		= showMacZp_DataPtr	; byte
setButtonColorZp_Ptr		= showMacZp_DataPtr+1	; word

;shared locations with showHelp:
byte2petZp_Col			= showMacZp_DataPtr	; byte
byte2petZp_Ptr			= showMacZp_DataPtr+1	; word
byte2petZp_0digit               = showMacZp_DataPtr+3   ; byte

;shared locations with showHelp:
selectItemZp_Ptr		= showMacZp_DataPtr	; word


;shared locations for screen init
showLogoZp_RCnt			= SelectedInputItem	; byte
showLogoZp_PCnt			= SelectedInputItem+1	; byte
showLogoZp_Col			= SelectedInputItem+2	; byte
showMenuZp_SBitCnt		= SelectedInputItem+3	; byte
showMenuZp_SByte		= SelectedInputItem+4	; byte
showMenuZp_ScreenPtr		= SelectedInputItem+5	; word
showMenuZp_PackedPtr		= SelectedInputItem+7	; word

; shared locations for magic detect
magicOk 			= SelectedInputItem+9	; byte
;-----------------------------
