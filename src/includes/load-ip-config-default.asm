
; @access public
; @uses word ipconfig
; @sets ActCfg_Mac
; @sets ActCfg_Ip
; @sets ActCfg_NetMask
; @sets ActCfg_Router
; @sets ActCfg_Nameserver
; @return void
loadIpConfigDefault
	lda #4
	ldx $ba
	ldy #0
	jsr KERNAL_SETLFS
	lda #16
	ldx #<ipconfig
	ldy #>ipconfig
	jsr KERNAL_SETNAM
	jsr KERNAL_OPEN
	lda #4
	jsr KERNAL_CLOSE
	lda $90
	bne noc
	jsr KERNAL_OPEN
	ldx #4
	jsr KERNAL_CHKIN
	jsr KERNAL_CHRIN
	cmp #$ec
	bne noc
	jsr KERNAL_CHRIN
	cmp #$64
	bne noc
gloop
	jsr KERNAL_CHRIN
	beq noc
	tax
	jsr KERNAL_CHRIN
	tay
	cpx #2
	bne +
	ldx #250
-
	jsr KERNAL_CHRIN
	sta ActCfg_Mac - 250,x
	inx
	bne -
	geq gloop

+
	cpx #3
	bne +
	ldx #252
-
	jsr KERNAL_CHRIN
	sta ActCfg_Ip - 252,x
	inx
	bne -
	geq gloop

+
	cpx #4
	bne +
	ldx #252
-
	jsr KERNAL_CHRIN
	sta ActCfg_NetMask - 252,x
	inx
	bne -
	geq gloop

+
	cpx #5
	bne +
	ldx #252
-
	jsr KERNAL_CHRIN
	sta ActCfg_Router - 252,x
	inx
	bne -
	geq gloop

+
	cpx #6
	bne +
	ldx #252
-
	jsr KERNAL_CHRIN
	sta ActCfg_Nameserver - 252,x
	inx
	bne -
	geq gloop
+
	dey
	dey
-
	jsr KERNAL_CHRIN
	dey
	bne -
	geq gloop

noc
	jsr KERNAL_CLRCHN
	lda #4
	jsr KERNAL_CLOSE
rts
