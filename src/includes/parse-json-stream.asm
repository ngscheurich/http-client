
; @access public
; @return void		
prepareJsonStream
	; When end of response data from TCP connection will be reached, Http-Load library will use stack pointer to exit
	tsx
	stx getbyte.vg + 1
rts


; Load current byte to accumulator
; Note: If Y register is used in this method, you have to save it, as JSON Parser uses it as well
; 
; @override
; @return A
JsonParser.getByte
	lda buffer
rts


; Read next STREAM_BUFFER_SIZE bytes
; Note: If Y register is used in this method, you have to save it, as JSON Parser uses it as well
; 
; @override
; @return void
JsonParser.walkForward
	jsr readFromStream
rts


; Reading from stream
; 
; @access public
; @return void
readFromStream
	.if MOCK_HTTP_REQUEST
		jmp readFromStreamMock
	.fi
	
	jsr getbyte
	sta buffer
	
	.if DEBUG_MODE
		ldx responseDisplayIdx
		jsr JsonParser.convertAsciiToPetscii
		sta SCREEN_LOCATION + 40 * 8,x
		inc responseDisplayIdx
	.fi
rts


.if MOCK_HTTP_REQUEST

; Simulate reading from stream
; 
; @access public
; @return void
readFromStreamMock
	tya
	pha
	
	ldy #0
	lda (streamSource),y
	sta buffer
	
	clc
	lda streamSource
	adc #1
	sta streamSource
	lda streamSource + 1
	adc #0
	sta streamSource + 1
	
	pla
	tay
rts

.fi
