
.enc ascii
.cdef 32, 126, 32 ; identity translation 32-126 to 32-126

; URI
hiScoresUrl .text "www.commocore.com/commodorex-api/hi-scores",13

; IP Config file to load
ipconfig .text "IPCONFIG-DEFAULT"


; User-Agent
httpUserAgent = "Commocore/HTTP"

.enc none


buffer = $30 ; byte

.if MOCK_HTTP_REQUEST
	streamSource = $31 ; word
.fi

.if DEBUG_MODE
	screen = $80 ; word
	number1 = $82
	number10 = $83
	number100 = $84
	stopperCounter = $85
	stopperWaiter = $86
	responseDisplayIdx = $87 ; used to display raw HTTP response on the screen
.fi
