
; @access public
; @return void
displayIp

	lda #<SCREEN_LOCATION + 40 * 0
	sta screen
	lda #>SCREEN_LOCATION + 40 * 0
	sta screen+1
	
	ldy #0

	lda ActCfg_Ip
	jsr extract8bitValueToDec
	jsr displayValueAsDec
	
	jsr displayDot
	
	lda ActCfg_Ip+1
	jsr extract8bitValueToDec
	jsr displayValueAsDec
	
	jsr displayDot
	
	lda ActCfg_Ip+2
	jsr extract8bitValueToDec
	jsr displayValueAsDec
	
	jsr displayDot
	
	lda ActCfg_Ip+3
	jsr extract8bitValueToDec
	jsr displayValueAsDec
	
rts


; @access public
; @return void
displayNetMask
	lda #<SCREEN_LOCATION + 40 * 1
	sta screen
	lda #>SCREEN_LOCATION + 40 * 1
	sta screen+1
	
	ldy #0

	lda ActCfg_NetMask
	jsr extract8bitValueToDec
	jsr displayValueAsDec
	
	jsr displayDot
	
	lda ActCfg_NetMask+1
	jsr extract8bitValueToDec
	jsr displayValueAsDec
	
	jsr displayDot
	
	lda ActCfg_NetMask+2
	jsr extract8bitValueToDec
	jsr displayValueAsDec
	
	jsr displayDot
	
	lda ActCfg_NetMask+3
	jsr extract8bitValueToDec
	jsr displayValueAsDec
rts


; @access public
; @return void
displayRouter
	lda #<SCREEN_LOCATION + 40 * 2
	sta screen
	lda #>SCREEN_LOCATION + 40 * 2
	sta screen+1
	
	ldy #0

	lda ActCfg_Router
	jsr extract8bitValueToDec
	jsr displayValueAsDec
	
	jsr displayDot
	
	lda ActCfg_Router+1
	jsr extract8bitValueToDec
	jsr displayValueAsDec
	
	jsr displayDot
	
	lda ActCfg_Router+2
	jsr extract8bitValueToDec
	jsr displayValueAsDec
	
	jsr displayDot
	
	lda ActCfg_Router+3
	jsr extract8bitValueToDec
	jsr displayValueAsDec
rts


; @access public
; @return void
displayNameserver
	lda #<SCREEN_LOCATION + 40 * 3
	sta screen
	lda #>SCREEN_LOCATION + 40 * 3
	sta screen+1
	
	ldy #0

	lda ActCfg_Nameserver
	jsr extract8bitValueToDec
	jsr displayValueAsDec
	
	jsr displayDot
	
	lda ActCfg_Nameserver+1
	jsr extract8bitValueToDec
	jsr displayValueAsDec
	
	jsr displayDot
	
	lda ActCfg_Nameserver+2
	jsr extract8bitValueToDec
	jsr displayValueAsDec
	
	jsr displayDot
	
	lda ActCfg_Nameserver+3
	jsr extract8bitValueToDec
	jsr displayValueAsDec
rts


; @access public
; @param A - value
; @affects number_1
; @affects number_10
; @affects number_100
; @return void
extract8bitValueToDec

	pha

	lda #0
	sta number100
	sta number10
	sta number1

	pla

	; hundreds
	sec
-
		cmp #100
		bcc +
		sbc #100
		inc number100
		jmp -
+

	; tens
	sec
-
		cmp #10
		bcc +
		sbc #10
		inc number10
		jmp -
+

	; ones
	clc
	sta number1

rts


; @access public
; @uses byte number1
; @uses byte number10
; @uses byte number100
; @return void
displayValueAsDec
	clc
	lda number100
	bne +
		lda number10
		bne ++
			jmp +++
+
	adc #48
	sta (screen),y
	iny
	lda number10
+
	adc #48
	sta (screen),y
	iny
+
	lda number1
	adc #48
	sta (screen),y
	iny
rts


; @access public
; @return void
displayDot
	lda #46
	sta (screen),y
	iny
rts


; @access public
; @param A - stage identifier
; @return void
stopper
	sta stopperCounter
		
	lda #0
	sta stopperWaiter

	lda #<SCREEN_LOCATION + 960
	sta screen
	lda #>SCREEN_LOCATION + 960
	sta screen+1
	lda stopperCounter
	ldy #0
	jsr extract8bitValueToDec
	jsr displayValueAsDec
-
	lda #%01111111
	sta $dc00
	lda $dc01
	and #%00010000
	bne +
		jmp -
+

-
	inc stopperWaiter
	lda stopperWaiter
	cmp #122
	bcs +
		lda #43
		.byte $2c
+
	lda #45

	sta (screen),y

	lda #%01111111
	sta $dc00
	lda $dc01
	and #%00010000
	bne -
rts


; @access public
; @return void
clearOutputScreen
	ldx #39
-
	lda #32
	.for line = 4, line < 23, line = line +1
		sta SCREEN_LOCATION + 40 * line,x
	.next
	
	dex
	bpl -
rts


stopperCmd .macro indicator
	lda #\indicator
	jsr stopper
.endm


; Only for test purposes to get JSON without calling over HTTP
.enc ascii
.cdef 32, 126, 32
mockedJsonResponse
	.text '[{"player":"gribbly","value":15954,"created":"2017-11-18 18:27:27"},{"player":"giana","value":7800,"created":"2017-11-18 19:42:00"}]'
.enc none

