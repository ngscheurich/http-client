	*=$0801
	.word ss,10
	.null $9e,^start
ss	.word 0

	*=$080e
	
start

	; Open IPCONFIG-DEFAULT file
	jsr loadIpConfigDefault
	
	jsr cleanScreen
	jsr initIrq

	; Display settings loaded from configuration
	.if DEBUG_MODE
		jsr displayIp
		jsr displayNetMask
		jsr displayRouter
		jsr displayNameserver
	.fi
	
	; Set URI of the request
	jsr setUri
	
-
	; Do HTTP request
	jsr executeHttpRequest
	
	.if DEBUG_MODE
		jmp -
	.fi

jmp *


; @access public
; @return void
executeHttpRequest

	.if DEBUG_MODE
		lda #0
		sta stopperCounter
		sta responseDisplayIdx
		
		#stopperCmd 1
		
		lda #6
		sta $d020
		
		jsr clearOutputScreen
	.fi
	
	.if MOCK_HTTP_REQUEST
		; Set pointers to JSON object which imitates data taken from HTTP request
		lda #<mockedJsonResponse
		sta streamSource
		lda #>mockedJsonResponse
		sta streamSource + 1
	.else
		; Send HTTP request
		.if REQUEST_METHOD = 'GET'
			jsr sendGetRequest
		.elsif REQUEST_METHOD = 'POST'
			jsr createPostRequestBodyForNewScore
			jsr sendPostRequest
		.fi
	.fi
	
	; Parse HTTP response
	jsr prepareJsonStream
	jsr hiScoresJsonParse
	
	.if MOCK_HTTP_REQUEST
		jmp retrieveDataEnd
	.fi
		
	jsr tcp_disconnect
	bcs retrieveDataEnd

	ldy DstIpIdx
	jsr ipCache_Free
		
retrieveDataEnd
	
	.if DEBUG_MODE
		lda #5
		sta $d020
	.fi
	
rts


; Send GET request
; 
; @access public
; @return void
sendGetRequest
	jsr HttpLoad
rts


; Send POST request
; 
; @access public
; @return carry flag set on error
sendPostRequest
	.if !HTTP_SEND_POST_BUFFER_TEST_LOCATION
		jsr initLayers
		jsr parseHostName
	.fi
	jsr HttpPostMethod
rts


.enc ascii
.cdef 32, 126, 32 ; identity translation 32-126 to 32-126

	; Include HTTP-Load library
	.include "includes/http/http64prg.asm"
	.include "includes/http/include.asm"
	.include "includes/http/s_data.asm"
	
	; Include POST method not available in HTTP-Load library
	.include "includes/http-post/http-send-post.asm"
	
	; Dynamic functionality to create body request for POST
	.include "includes/create-post-request-body-for-new-score.asm"
	
.enc none

	.include "includes/constants.asm"
	.include "includes/variables.asm"
	.include "includes/init-irq.asm"
	.include "includes/irq.asm"
	.include "includes/load-ip-config-default.asm"
	.include "includes/set-uri.asm"
	.include "includes/clean-screen.asm"
	.include "includes/parse-json-stream.asm"
	.include "includes/hi-scores-json-parse.asm"
	
	; Include 6502 JSON Parser library
	.include "../vendor/6502-json-parser/src/json-parser.asm"

	.if DEBUG_MODE
		.include "includes/debug.asm"
	.fi
